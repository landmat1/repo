SELECT DISTINCT
r.ROUTE_ID,
r.OO_RouteEventID,
r.LINE_LineName,
r.ROUTE_MasterSegmentEventID,
r.ROUTE_BeginMeasure,
r.ROUTE_EndMeasure,
r.ROUTE_OperationalStatus,
BusinessUnit,
OO_OwnerCL,
OO_OperatorCL,
r.LINE_LineFunction,
mat.Material,
min_inst.MIN_InstallationDate,
pt.TEST_DATE AS Pressure_Test_Date,
hca.TypeCL
FROM STG1.PODS_OPERATING_ROUTE_VW r
LEFT JOIN STG2.DL_MOST_RECENT_PT_RANGE pt ON pt.ROUTE_ID = r.ROUTE_ID
LEFT JOIN
    (
    SELECT
    ROUTE_ID,
    Material
    FROM STG2.DL_PIPESEGMENT
    GROUP BY ROUTE_ID, Material
    ) mat ON mat.ROUTE_ID = r.ROUTE_ID
LEFT JOIN
 (
  SELECT
       [RouteEventID]
      ,[TypeCL]
    FROM [STG1].[PODS_HCABOUNDARY]
    GROUP BY RouteEventID, TypeCL
 ) hca ON hca.RouteEventID = r.ROUTE_EventID
LEFT JOIN
 (
   SELECT
   [ROUTE_ID]
   ,MIN(YEAR(InstallationDate)) as MIN_InstallationDate
   FROM [STG2].DL_PIPESEGMENT
   GROUP BY ROUTE_ID
 ) as min_inst ON min_inst.ROUTE_ID = r.ROUTE_ID

WHERE 
--ROUTE must have steel pipe
Material = 'STEEL' 
--ROUTE must be Transmission class pipe
and LINE_LineFunction = 'T'
--ROUTE must not be failed in STG1
AND OO_FAILED = 0 
--ROUTE must be a NG pipe
AND r.BusinessUnit = 'NATURAL GAS'
--Toggle by Owner if desired
--AND OO_OwnerCL = 'SNG' 
--There is no Pressure Test Date
AND pt.TEST_DATE IS NULL 
--ROUTE must be installed after 1970
AND MIN_InstallationDate > 1970
--ROUTE must contain some HCA mileage
AND TypeCL = 'METHOD 2' 
--ROUTE must be longer than 1000ft
AND ROUTE_EndMeasure > 1000
--ROUTE must be ACTIVE status
AND ROUTE_OperationalStatus = 'ACTIVE'

ORDER BY ROUTE_EndMeasure DESC