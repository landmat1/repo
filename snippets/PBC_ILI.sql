INSERT INTO STG2.DL_PIPE_BODY_COR_CALCS (PIPE_BODY_ID, CALC_MODE, OutsideDiameter, NominalWallThickness, SMYS, InstallationDate, JOINT_FACTOR, DESIGN_PRESSURE_PSIG, MAOP, PIPE_AGE, YEARS_SINCE_LAST_ILI, REPAIR_STATUS, AnomalyType, Max_or_PeakDepthGrowthRate_INT, Max_or_PeakDepthGrowthRate_EXT, DefectDepth, DefectLength, ClockPosition_Degree_Delta)

--TRUNCATE TABLE STG2.DL_PIPE_BODY_COR_CALCS
SELECT
pb.ID AS PIPE_BODY_ID,
'HAS ILI' AS CALC_MODE,
ps.OutsideDiameter,
ps.NominalWallThickness,
ps.SMYS,
ps.InstallationDate,
ps.JOINT_FACTOR,
ps.DESIGN_PRESSURE_PSIG,
ip.P_MAX AS MAOP,
ps.PIPE_AGE,
CASE WHEN  ili_er.ILI_RUN_DATE IS NOT NULL THEN datediff(day, ili_er.ILI_RUN_DATE, GETDATE())/365.0
	 WHEN  ili_ir.ILI_RUN_DATE IS NOT NULL THEN datediff(day, ili_ir.ILI_RUN_DATE, GETDATE())/365.0
	 ELSE NULL
	 END
AS YEARS_SINCE_LAST_ILI,
CASE WHEN aa_e.PIStatusCL = 'PI COMPLETE' THEN 'REPAIRED'
WHEN aa_i.PIStatusCL = 'PI COMPLETE' THEN 'REPAIRED'
END as REPAIR_STATUS,
CASE WHEN ili_ef.KMAnomalyTypeCL IS NULL AND ili_if.KMAnomalyTypeCL IS NOT NULL THEN ili_if.KMAnomalyTypeCL
	WHEN ili_if.KMAnomalyTypeCL IS NULL AND ili_ef.KMAnomalyTypeCL IS NOT NULL THEN ili_ef.KMAnomalyTypeCL
	WHEN ili_if.KMAnomalyTypeCL IS NOT NULL AND ili_ef.KMAnomalyTypeCL IS NOT NULL THEN 'BOTH INT AND EXT'
	ELSE NULL
	END AS AnomalyType,

CASE WHEN addtnl_i.MaxGrowthRateInCluster < addtnl_i.PeakDepthGrowthRate THEN addtnl_i.PeakDepthGrowthRate
	 WHEN addtnl_i.MaxGrowthRateinCluster > addtnl_i.PeakDepthGrowthRate THEN addtnl_i.MaxGrowthRateinCluster
	 END 
AS Max_or_PeakDepthGrowthRate_INT,
CASE WHEN addtnl_e.MaxGrowthRateInCluster < addtnl_e.PeakDepthGrowthRate THEN addtnl_e.PeakDepthGrowthRate
	 WHEN addtnl_e.MaxGrowthRateinCluster > addtnl_e.PeakDepthGrowthRate THEN addtnl_e.MaxGrowthRateinCluster
	 END 
AS Max_or_PeakDepthGrowthRate_EXT,

CASE WHEN aa_e.PIStatusCL = 'PI COMPLETE' or aa_i.PIStatusCL = 'PI COMPLETE' THEN NULL
	 WHEN
			ABS(CASE WHEN LEN(ili_if.OrientationOclock) = 5 THEN
						--Sometimes they put 12:30 instead of O:30 for topside clock position, causing degree angle to be greater than 360
						CASE WHEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5-360
							ELSE  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5
							END
				WHEN LEN(ili_if.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5-360
							ELSE SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END  -
			CASE WHEN LEN(ili_ef.OrientationOclock) = 5 THEN 
						CASE WHEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 - 360
							 ELSE SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5
							 END
				 WHEN LEN(ili_ef.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 - 360
							ELSE SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END )
 >= 330
 OR
			ABS(CASE WHEN LEN(ili_if.OrientationOclock) = 5 THEN
						--Sometimes they put 12:30 instead of O:30 for topside clock position, causing degree angle to be greater than 360
						CASE WHEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5-360
							ELSE  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5
							END
				WHEN LEN(ili_if.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5-360
							ELSE SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END  -
			CASE WHEN LEN(ili_ef.OrientationOclock) = 5 THEN 
						CASE WHEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 - 360
							 ELSE SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5
							 END
				 WHEN LEN(ili_ef.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 - 360
							ELSE SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END ) 
 <= 30
 --BOTH EXT AND INT ML ON SAME MEASURE, SAME CLOCK POSITION ARE COMBINED DEPTHS
 THEN (ili_if.MaxDepthPCT/100 *ps.NominalWallThickness)  + (ili_ef.MaxDepthPCT/100 * ps.NominalWallThickness)
 --BOTH EXT AND INT ML ON SAME MEASURE, DIFFERENT CLOCK POSITION - CHOOSE THE LONGEST FEATURE
WHEN ili_if.KMAnomalyTypeCL IS NOT NULL AND ili_ef.KMAnomalyTypeCL IS NOT NULL AND  ili_if.Length > ili_ef.Length THEN ili_if.MaxDepthPCT/100 * ps.NominalWallThickness
WHEN ili_if.KMAnomalyTypeCL IS NOT NULL AND ili_ef.KMAnomalyTypeCL IS NOT NULL AND  ili_if.Length < ili_ef.Length THEN ili_ef.MaxDepthPCT/100 * ps.NominalWallThickness
--EXT OR INT ML NOT ON SAME MEASURE
WHEN ili_if.KMAnomalyTypeCL IS NOT NULL THEN ili_if.MaxDepthPCT/100 * ps.NominalWallThickness
WHEN ili_ef.KMAnomalyTypeCL IS NOT NULL THEN ili_ef.MaxDepthPCT/100 * ps.NominalWallThickness
END
AS  DefectDepth,

CASE  WHEN aa_e.PIStatusCL = 'PI COMPLETE' or aa_i.PIStatusCL = 'PI COMPLETE' THEN NULL
	  WHEN	ABS(CASE WHEN LEN(ili_if.OrientationOclock) = 5 THEN
						--Sometimes they put 12:30 instead of O:30 for topside clock position, causing degree angle to be greater than 360
						CASE WHEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5-360
							ELSE  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5
							END
				WHEN LEN(ili_if.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5-360
							ELSE SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END  -
			CASE WHEN LEN(ili_ef.OrientationOclock) = 5 THEN 
						CASE WHEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 - 360
							 ELSE SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5
							 END
				 WHEN LEN(ili_ef.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 - 360
							ELSE SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END )
 >= 330
 OR
			ABS(CASE WHEN LEN(ili_if.OrientationOclock) = 5 THEN
						--Sometimes they put 12:30 instead of O:30 for topside clock position, causing degree angle to be greater than 360
						CASE WHEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5-360
							ELSE  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5
							END
				WHEN LEN(ili_if.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5-360
							ELSE SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END  -
			CASE WHEN LEN(ili_ef.OrientationOclock) = 5 THEN 
						CASE WHEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 - 360
							 ELSE SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5
							 END
				 WHEN LEN(ili_ef.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 - 360
							ELSE SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END ) 
 <= 30
 THEN ili_if.Length + ili_ef.Length
 WHEN ili_if.KMAnomalyTypeCL IS NOT NULL AND ili_ef.KMAnomalyTypeCL IS NOT NULL AND  ili_if.Length > ili_ef.Length THEN ili_if.Length
WHEN ili_if.KMAnomalyTypeCL IS NOT NULL AND ili_ef.KMAnomalyTypeCL IS NOT NULL AND  ili_if.Length < ili_ef.Length THEN ili_ef.Length
WHEN ili_if.KMAnomalyTypeCL IS NOT NULL THEN ili_if.Length
WHEN ili_ef.KMAnomalyTypeCL IS NOT NULL THEN ili_ef.Length
END as DefectLength,

ABS(CASE WHEN LEN(ili_if.OrientationOclock) = 5 THEN
						--Sometimes they put 12:30 instead of O:30 for topside clock position, causing degree angle to be greater than 360
						CASE WHEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5-360
							ELSE  SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_if.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_if.OrientationOclock, 4, 5))) as INT) * 0.5
							END
				WHEN LEN(ili_if.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5-360
							ELSE SUBSTRING(ili_if.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_if.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_if.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_if.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END  -
			CASE WHEN LEN(ili_ef.OrientationOclock) = 5 THEN 
						CASE WHEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 > 360 THEN SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5 - 360
							 ELSE SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 1, 2),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 1, 2)),LEN(SUBSTRING(ili_ef.OrientationOclock, 1, 2))) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 4, 5),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 4, 5)),LEN(SUBSTRING(ili_ef.OrientationOclock, 4, 5))) as INT) * 0.5
							 END
				 WHEN LEN(ili_ef.OrientationOclock) = 4 THEN 
						CASE WHEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 > 360 THEN SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5 - 360
							ELSE SUBSTRING(ili_ef.OrientationOclock, 1,1) * 30 + CAST(SUBSTRING(SUBSTRING(ili_ef.OrientationOclock, 3, 4),PATINDEX('%[^0]%', SUBSTRING(ili_ef.OrientationOclock, 3, 4)),LEN(SUBSTRING(ili_ef.OrientationOclock, 3, 4))) as INT) * 0.5
							END
				 ELSE NULL
				END )
AS ClockPosition_Degree_Delta
--ili_if.MaxDepthPCT AS INT_MaxDepthPCT,
--ili_if.Length AS INT_Length,
--ili_ef.MaxDepthPCT AS EXT_MaxDeptchPCT,
--ili_ef.Length AS EXT_Length


FROM STG2.DL_PIPE_BODY pb
LEFT JOIN [STG2].[DL_PIPESEGMENT] ps								ON ps.ID = pb.PIPESEGMENT_ID
LEFT JOIN [STG2].DL_FLATTENED_MOST_RECENT_ILIDATA_INT_RANGE ili_ir  ON ili_ir.ILIDATA_ID = pb.INT_ILIDATA_ID
   LEFT JOIN [STG1].INTEGRITY_ILIDATA ili_if						ON ili_if.ID = ili_ir.ILIDATA_ID
   LEFT JOIN [STG1].INTEGRITY_ILIDATA_ADDITIONAL addtnl_i				ON addtnl_i.EventID = ili_if.EventID
LEFT JOIN [STG2].DL_FLATTENED_MOST_RECENT_ILIDATA_EXT_RANGE ili_er	ON ili_er.ILIDATA_ID = pb.EXT_ILIDATA_ID
   LEFT JOIN [STG1].INTEGRITY_ILIDATA ili_ef						ON ili_ef.ID = ili_er.ILIDATA_ID
   LEFT JOIN [STG1].INTEGRITY_ILIDATA_ADDITIONAL addtnl_e				ON addtnl_e.EventID = ili_ef.EventID
LEFT JOIN STG2.DL_PIPE_BODY_COR_CALCS pbc							ON pbc.PIPE_BODY_ID = pb.ID
LEFT JOIN STG1.INTEGRITY_ACTIONABLEANOMALY aa_e						ON aa_e.ILIDataEventID = ili_ef.EventID
LEFT JOIN STG1.INTEGRITY_ACTIONABLEANOMALY aa_i						ON aa_i.ILIDataEventID = ili_if.EventID
LEFT JOIN [STG3].DIM_INTERNAL_PRESSURE_RANGE ipr					ON ipr.ID = pb.INTERNAL_PRESSURE_ID
LEFT JOIN [STG3].DIM_INTERNAL_PRESSURE ip							ON ip.[KEY] = ipr.INTERNAL_PRESSURE_KEY
LEFT JOIN [STG2].DL_MOST_RECENT_PT_RANGE pt							ON pt.ID = pb.PRESSURE_TEST_ID
LEFT JOIN [STG2].DL_MOST_RECENT_ILI_TECHNOLOGY_RANGE ili_r		    ON ili_r.ID = pb.ILI_INSPECTION_RANGE_ID
WHERE ili_r.ID IS NOT NULL AND (ili_ef.ID IS NOT NULL OR ili_if.ID IS NOT NULL)

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET ILI_COR_IPY =
	CASE WHEN AnomalyType = 'INT ML' 
			THEN
				--no addtnl, use install date
				CASE WHEN Max_or_PeakDepthGrowthRate_INT IS NULL THEN DefectDepth / PIPE_AGE 
					--addtnl shows no defect growth, set to 0 IPY
					WHEN Max_or_PeakDepthGrowthRate_INT < 0 THEN 0
					--where addtnl is avail, use addtnl
					ELSE Max_or_PeakDepthGrowthRate_INT/1000
				END
		WHEN AnomalyType = 'EXT ML' 
			THEN
				--no addtnl, use install date
				CASE WHEN Max_or_PeakDepthGrowthRate_EXT IS NULL THEN DefectDepth / PIPE_AGE 
					--addtnl shows no defect growth, set to 0 IPY
					WHEN Max_or_PeakDepthGrowthRate_EXT < 0 THEN 0
					--where addtnl is avail, use addtnl
					ELSE Max_or_PeakDepthGrowthRate_EXT/1000
				END
		--EXT & INT ML ON SAME MEASURE, SAME CLOCK POSITION, COMBINE THEIR IPY RATES
		WHEN AnomalyType = 'BOTH INT AND EXT' AND (ClockPosition_Degree_Delta <= 30 OR ClockPosition_Degree_Delta >= 330)
			THEN
			   CASE WHEN Max_or_PeakDepthGrowthRate_EXT IS NULL AND Max_or_PeakDepthGrowthRate_INT IS NULL then DefectDepth / PIPE_AGE
				   WHEN Max_or_PeakDepthGrowthRate_EXT < 0 AND Max_or_PeakDepthGrowthRate_INT < 0 THEN 0
				   WHEN Max_or_PeakDepthGrowthRate_EXT IS NULL THEN Max_or_PeakDepthGrowthRate_INT/1000
				   WHEN Max_or_PeakDepthGrowthRate_INT IS NULL THEN Max_or_PeakDepthGrowthRate_EXT/1000
				   ELSE Max_or_PeakDepthGrowthRate_INT/1000 + Max_or_PeakDepthGrowthRate_EXT/1000
			   END
		--EXT & INT ML ON SAME MEASURE, DIFFERENT CLOCK POSITION - CHOOSE LARGEST IPY RATE
		WHEN AnomalyType = 'BOTH INT AND EXT' AND (ClockPosition_Degree_Delta > 30 OR ClockPosition_Degree_Delta < 330)
			THEN
			   CASE WHEN Max_or_PeakDepthGrowthRate_EXT IS NULL AND Max_or_PeakDepthGrowthRate_INT IS NULL then DefectDepth / PIPE_AGE
				   WHEN Max_or_PeakDepthGrowthRate_EXT < 0 AND Max_or_PeakDepthGrowthRate_INT < 0 THEN 0
				   WHEN Max_or_PeakDepthGrowthRate_EXT IS NULL THEN Max_or_PeakDepthGrowthRate_INT/1000
				   WHEN Max_or_PeakDepthGrowthRate_INT IS NULL THEN Max_or_PeakDepthGrowthRate_EXT/1000
				   WHEN Max_or_PeakDepthGrowthRate_INT > Max_or_PeakDepthGrowthRate_EXT THEN Max_or_PeakDepthGrowthRate_INT/1000
				   ELSE Max_or_PeakDepthGrowthRate_EXT/1000
			   END
END
FROM STG2.DL_PIPE_BODY_COR_CALCS

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET DEFECT_DEPTH_CURRENT = 
pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)
FROM [STG2].[DL_PIPE_BODY_COR_CALCS] pbc
WHERE ILI_COR_IPY IS NOT NULL

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET RATIO_DEFECT_DEPTH_TO_LENGTH =
pbc.DefectLength / pbc.DefectDepth --RATIO_DEFECT_DEPTH_TO_LENGTH
FROM STG2.DL_PIPE_BODY_COR_CALCS pbc
WHERE ILI_COR_IPY IS NOT NULL

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET DEFECT_LENGTH_CURRENT =
pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)) --DEFECT_LENGTH_CURRENT
FROM STG2.DL_PIPE_BODY_COR_CALCS pbc
WHERE ILI_COR_IPY IS NOT NULL

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET LEN_DT =
POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness) --LEN_DT
FROM STG2.DL_PIPE_BODY_COR_CALCS pbc
WHERE ILI_COR_IPY IS NOT NULL

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET FOLIAS_FACTOR =
	CASE WHEN POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness) > 50
		THEN 0.032*(POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness)) + 3.3
	 ELSE 
	 POWER(1 + (0.6275 * (POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness))) - (0.003375 * POWER((POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness)),2)), 0.5)
	 END
	 --FOLIAS FACTOR
FROM STG2.DL_PIPE_BODY_COR_CALCS pbc
WHERE ILI_COR_IPY IS NOT NULL

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET MB31G =
  --ORDER OF OPERATIONS IS SENSITIVE
  (pbc.SMYS+10000) * --SMYS10K
 (1-0.85*(pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY))/pbc.NominalWallThickness) / --TOP HALF OF EQUATION
 --BOTTOM HALF OF EQUATION
 ( 
	1-((0.85*(pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)))/pbc.NominalWallThickness)*
	  (1/
		CASE WHEN POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness) > 50
			THEN 0.032*(POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness)) + 3.3
		 ELSE 
		 POWER(1 + (0.6275 * (POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness))) - (0.003375 * POWER((POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness)),2)), 0.5)
		 END
	  )
 )
FROM STG2.DL_PIPE_BODY_COR_CALCS pbc
WHERE ILI_COR_IPY IS NOT NULL


UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET DESIGN_PRESSURE_PSIG_CURRENT =
2*
--MB31G
(
  (pbc.SMYS+10000) * --SMYS10K
 (1-0.85*(pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY))/pbc.NominalWallThickness) / --TOP HALF OF EQUATION
 --BOTTOM HALF OF EQUATION
 ( 
	1-((0.85*(pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)))/pbc.NominalWallThickness)*
	  (1/
		CASE WHEN POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness) > 50
			THEN 0.032*(POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness)) + 3.3
		 ELSE 
		 POWER(1 + (0.6275 * (POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness))) - (0.003375 * POWER((POWER(pbc.DefectLength / pbc.DefectDepth * (pbc.DefectDepth + (YEARS_SINCE_LAST_ILI * pbc.ILI_COR_IPY)),2)  / (pbc.OutsideDiameter * pbc.NominalWallThickness)),2)), 0.5)
		 END
	  )
 )
)
		
*pbc.NominalWallThickness/pbc.OutsideDiameter*pbc.JOINT_FACTOR
FROM [STG2].[DL_PIPE_BODY_COR_CALCS] pbc
WHERE ILI_COR_IPY IS NOT NULL

--THIS IS NECESSARY TO SETUP THE TTF LOOP PROPERLY
UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET DESIGN_PRESSURE_PSIG_FUTURE =
DESIGN_PRESSURE_PSIG_CURRENT
FROM [STG2].[DL_PIPE_BODY_COR_CALCS]
WHERE ILI_COR_IPY IS NOT NULL

