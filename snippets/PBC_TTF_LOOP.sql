
-----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
--UNIVERSAL CALCS COMMON TO ILI, NONILI, PT--

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET COR_IPY = 
CASE WHEN ILI_COR_IPY IS NOT NULL THEN ILI_COR_IPY
	 WHEN MODELED_COR_IPY_AT_TEST_ADJ IS NOT NULL THEN MODELED_COR_IPY_AT_TEST_ADJ
	 WHEN MODELED_COR_IPY IS NOT NULL THEN MODELED_COR_IPY
	 ELSE NULL
	 END
FROM STG2.DL_PIPE_BODY_COR_CALCS

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET TTF = 0
WHERE DESIGN_PRESSURE_PSIG_CURRENT < MAOP

UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET DESIGN_PRESSURE_PSIG_50YR =
2*
			--MB31G
			(
			  (SMYS+10000) * --SMYS10K
			 (1-0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * 50))/NominalWallThickness) / --TOP HALF OF EQUATION
			 --BOTTOM HALF OF EQUATION
			 ( 
				1-((0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * 50)))/NominalWallThickness)*
				  (1/
					CASE WHEN POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 50)),2)  / (OutsideDiameter * NominalWallThickness) > 50
						THEN 0.032*(POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 50)),2)  / (OutsideDiameter * NominalWallThickness)) + 3.3
					 ELSE 
					 POWER(1 + (0.6275 * (POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 50)),2)  / (OutsideDiameter * NominalWallThickness))) - (0.003375 * POWER((POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 50)),2)  / (OutsideDiameter * NominalWallThickness)),2)), 0.5)
					 END
				  )
			 )
			)
			*NominalWallThickness/OutsideDiameter*JOINT_FACTOR

FROM [STG2].[DL_PIPE_BODY_COR_CALCS] pbc


UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET DESIGN_PRESSURE_PSIG_100YR =
2*
			--MB31G
			(
			  (SMYS+10000) * --SMYS10K
			 (1-0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * 100))/NominalWallThickness) / --TOP HALF OF EQUATION
			 --BOTTOM HALF OF EQUATION
			 ( 
				1-((0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * 100)))/NominalWallThickness)*
				  (1/
					CASE WHEN POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 100)),2)  / (OutsideDiameter * NominalWallThickness) > 50
						THEN 0.032*(POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 100)),2)  / (OutsideDiameter * NominalWallThickness)) + 3.3
					 ELSE 
					 POWER(1 + (0.6275 * (POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 100)),2)  / (OutsideDiameter * NominalWallThickness))) - (0.003375 * POWER((POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 100)),2)  / (OutsideDiameter * NominalWallThickness)),2)), 0.5)
					 END
				  )
			 )
			)
			*NominalWallThickness/OutsideDiameter*JOINT_FACTOR

FROM [STG2].[DL_PIPE_BODY_COR_CALCS] pbc


UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET DESIGN_PRESSURE_PSIG_150YR =
2*
			--MB31G
			(
			  (SMYS+10000) * --SMYS10K
			 (1-0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * 150))/NominalWallThickness) / --TOP HALF OF EQUATION
			 --BOTTOM HALF OF EQUATION
			 ( 
				1-((0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * 150)))/NominalWallThickness)*
				  (1/
					CASE WHEN POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 150)),2)  / (OutsideDiameter * NominalWallThickness) > 50
						THEN 0.032*(POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 150)),2)  / (OutsideDiameter * NominalWallThickness)) + 3.3
					 ELSE 
					 POWER(1 + (0.6275 * (POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 150)),2)  / (OutsideDiameter * NominalWallThickness))) - (0.003375 * POWER((POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * 150)),2)  / (OutsideDiameter * NominalWallThickness)),2)), 0.5)
					 END
				  )
			 )
			)
			*NominalWallThickness/OutsideDiameter*JOINT_FACTOR

FROM [STG2].[DL_PIPE_BODY_COR_CALCS] pbc


UPDATE STG2.DL_PIPE_BODY_COR_CALCS
SET TIME_TO_LEAK = 
  (NominalWallThickness-DEFECT_DEPTH_CURRENT) / COR_IPY
  FROM [PI_ODS_DEV].[STG2].[DL_PIPE_BODY_COR_CALCS]
  WHERE COR_IPY > 0


declare @FutureYears INT
set @FutureYears = 1

	WHILE @FutureYears <= 50
	BEGIN
		 UPDATE STG2.DL_PIPE_BODY_COR_CALCS
		 SET DESIGN_PRESSURE_PSIG_FUTURE = 

				2*
				--MB31G
				(
				  (SMYS+10000) * --SMYS10K
				 (1-0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears))/NominalWallThickness) / --TOP HALF OF EQUATION
				 --BOTTOM HALF OF EQUATION
				 ( 
					1-((0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)))/NominalWallThickness)*
					  (1/
						CASE WHEN POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness) > 50
							THEN 0.032*(POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness)) + 3.3
						 ELSE 
						 POWER(1 + (0.6275 * (POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness))) - (0.003375 * POWER((POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness)),2)), 0.5)
						 END
					  )
				 )
				)
				*NominalWallThickness/OutsideDiameter*JOINT_FACTOR

		  FROM [STG2].[DL_PIPE_BODY_COR_CALCS]
		  WHERE DEFECT_DEPTH_CURRENT IS NOT NULL AND COR_IPY > 0 AND DESIGN_PRESSURE_PSIG_FUTURE > MAOP AND [DESIGN_PRESSURE_PSIG_50YR] < MAOP

		  UPDATE STG2.DL_PIPE_BODY_COR_CALCS
		  SET TTF = @FutureYears
		  FROM [STG2].[DL_PIPE_BODY_COR_CALCS]
		  WHERE DEFECT_DEPTH_CURRENT IS NOT NULL AND COR_IPY > 0 AND DESIGN_PRESSURE_PSIG_FUTURE > MAOP AND [DESIGN_PRESSURE_PSIG_50YR] < MAOP
		  
		  --INCREMENT THE COUNTER BEFORE LOOPING AGAIN
		  PRINT @FutureYears
		  SET @FutureYears = @FutureYears + 1
	END

	WHILE @FutureYears >= 51 AND @FutureYears <= 100
	BEGIN
		 UPDATE STG2.DL_PIPE_BODY_COR_CALCS
		 SET DESIGN_PRESSURE_PSIG_FUTURE = 

				2*
				--MB31G
				(
				  (SMYS+10000) * --SMYS10K
				 (1-0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears))/NominalWallThickness) / --TOP HALF OF EQUATION
				 --BOTTOM HALF OF EQUATION
				 ( 
					1-((0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)))/NominalWallThickness)*
					  (1/
						CASE WHEN POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness) > 50
							THEN 0.032*(POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness)) + 3.3
						 ELSE 
						 POWER(1 + (0.6275 * (POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness))) - (0.003375 * POWER((POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness)),2)), 0.5)
						 END
					  )
				 )
				)
				*NominalWallThickness/OutsideDiameter*JOINT_FACTOR

		  FROM [STG2].[DL_PIPE_BODY_COR_CALCS]
		  WHERE DEFECT_DEPTH_CURRENT IS NOT NULL AND COR_IPY > 0 AND DESIGN_PRESSURE_PSIG_FUTURE > MAOP AND [DESIGN_PRESSURE_PSIG_100YR] < MAOP

		  UPDATE STG2.DL_PIPE_BODY_COR_CALCS
		  SET TTF = @FutureYears
		  FROM [STG2].[DL_PIPE_BODY_COR_CALCS]
		  WHERE DEFECT_DEPTH_CURRENT IS NOT NULL AND COR_IPY > 0 AND DESIGN_PRESSURE_PSIG_FUTURE > MAOP AND [DESIGN_PRESSURE_PSIG_100YR] < MAOP
		  
		  --INCREMENT THE COUNTER BEFORE LOOPING AGAIN
		  PRINT @FutureYears
		  SET @FutureYears = @FutureYears + 1
	END

	WHILE @FutureYears >= 101 AND @FutureYears <= 150
	BEGIN
		 UPDATE STG2.DL_PIPE_BODY_COR_CALCS
		 SET DESIGN_PRESSURE_PSIG_FUTURE = 

				2*
				--MB31G
				(
				  (SMYS+10000) * --SMYS10K
				 (1-0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears))/NominalWallThickness) / --TOP HALF OF EQUATION
				 --BOTTOM HALF OF EQUATION
				 ( 
					1-((0.85*(DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)))/NominalWallThickness)*
					  (1/
						CASE WHEN POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness) > 50
							THEN 0.032*(POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness)) + 3.3
						 ELSE 
						 POWER(1 + (0.6275 * (POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness))) - (0.003375 * POWER((POWER(RATIO_DEFECT_DEPTH_TO_LENGTH * (DEFECT_DEPTH_CURRENT + (COR_IPY * @FutureYears)),2)  / (OutsideDiameter * NominalWallThickness)),2)), 0.5)
						 END
					  )
				 )
				)
				*NominalWallThickness/OutsideDiameter*JOINT_FACTOR

		  FROM [STG2].[DL_PIPE_BODY_COR_CALCS]
		  WHERE DEFECT_DEPTH_CURRENT IS NOT NULL AND COR_IPY > 0 AND DESIGN_PRESSURE_PSIG_FUTURE > MAOP AND [DESIGN_PRESSURE_PSIG_150YR] < MAOP

		  UPDATE STG2.DL_PIPE_BODY_COR_CALCS
		  SET TTF = @FutureYears
		  FROM [STG2].[DL_PIPE_BODY_COR_CALCS]
		  WHERE DEFECT_DEPTH_CURRENT IS NOT NULL AND COR_IPY > 0 AND DESIGN_PRESSURE_PSIG_FUTURE > MAOP AND [DESIGN_PRESSURE_PSIG_150YR] < MAOP
		  
		  --INCREMENT THE COUNTER BEFORE LOOPING AGAIN
		  PRINT @FutureYears
		  SET @FutureYears = @FutureYears + 1
	END

