
WITH
COUP as
(
	SELECT
		SamplePtID,
		ActualRemovalDate as EvaluationDate,
		CouponType,
		CorrosionRate,
		PitRate,
		PitType,
		PitTypeDescription,
		SEMCorrSeverity,
		OpticalSeverity,
		ROW_NUMBER() OVER (PARTITION BY SamplePtID ORDER BY ActualRemovalDate desc) AS rn
	FROM STG1.TSIMS_COUPON
	WHERE FAILED = 0
	AND ActualExposure >= 90
	AND ActualRemovalDate IS NOT NULL
),
SCORE as
(
SELECT
	d.TO_ROUTE_ID as ROUTE_ID,
	d.TO_MEASURE as Measure,
    r.EndMeasure,
    c.SamplePtID,
    c.EvaluationDate,
	max(CASE
		WHEN (c.CorrosionRate >= 5
		  or c.PitRate >=8
		  or (c.PitType in ('COMB', 'MIC', 'PM') and c.SEMCorrSeverity > 3))
		  THEN '2'
		WHEN (c.CorrosionRate >=1.5
		  or c.PitRate >= 5
		  or c.OpticalSeverity > 3)
		  THEN '1'
		ELSE '0'
		END) as OM906_SCORE
FROM COUP c
INNER JOIN STG1.TSIMS_HIERARCHY h
	ON c.SamplePtID = h.SamplePtID
INNER JOIN STG2.DL_TSIMS_PODS_DISPLACEMENT_VECTOR d
	ON h.ID = d.FROM_TSIMS_HIERARCHY_ID
INNER JOIN STG1.PODS_HIERARCHY_ROUTE r
	ON d.TO_ROUTE_ID = r.ID
WHERE c.rn = 1
  AND h.FAILED = 0
  AND r.FAILED = 0
  AND d.DISTANCE_FT <= 1320
GROUP BY d.TO_ROUTE_ID, d.TO_MEASURE, r.EndMeasure, c.EvaluationDate, c.SamplePtID
)
,
MILL AS (
SELECT
	s.ROUTE_ID,
	s.Measure,
	s.EndMeasure,
    s.SamplePtID,
	s.EvaluationDate,
	--lead(s.EvaluationDate, 1, GETDATE()) over (partition by s.ROUTE_ID, s.Measure order by s.EvaluationDate asc),
	--TIME BETWEEN READINGS (YEARS)
	ABS( --return only positive numbers
		datediff(
			     day, --return # of days
				 lead(s.EvaluationDate, 1, GETDATE()) over (partition by s.ROUTE_ID, s.Measure order by s.EvaluationDate asc), -- next reading
				 s.EvaluationDate -- current reading
			    )
		) 
		/ 365.00 --convert to years
		as YEARS_BETWEEN_READINGS,
	ABS(datediff(day,lead(s.EvaluationDate, 1, GETDATE()) over (partition by s.ROUTE_ID, s.Measure order by s.EvaluationDate asc),s.EvaluationDate))/ 365.00 * om.CORROSION_RATE as WALL_LOSS_MILL,
	om.OM906_SEVERITY,
	om.CORROSION_RATE
FROM SCORE s
INNER JOIN UTIL.LU_OM906_COUPON_CORROSION_RATE om
	ON om.OM906_SCORE = s.OM906_SCORE
WHERE ROUTE_ID = 4580
)
SELECT 
ROUTE_ID, Measure, AVG(WALL_LOSS_MILL) AS AVG_WALL_LOSS_MILL, MIN(EvaluationDate) as START_DATE
FROM MILL
GROUP BY ROUTE_ID, Measure

--Problem#1
---How to handle pipeline replacements during the span of time for which coupons were being read
--Problem#2
---We should be adding all the WALL_LOSS recorded and not generating some MPY_AVG
--Problem#3
---How to handle 50 year old pipelines for which only 10 years of readings are available. What to do with Years 0-40?