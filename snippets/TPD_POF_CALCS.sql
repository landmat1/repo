--TEST FOR DEV PURPOSES ONLY
--USED TO CALIBRATE AND INFORM DVM->RISK CALC MAPPING
WITH T1 AS (
SELECT
	   [ID]
      ,[ROUTE_ID]
      ,[BeginMeasure]
      ,[EndMeasure]
	   --EXCAV DEPTH EXCEED COVER
      ,[DepthFT]

	  ,CASE
			WHEN [AbovegroundLF] = '1' THEN 0
			WHEN [DepthFT] >=0 AND [DepthFT] <= 1 THEN 1
			WHEN [DepthFT] >=1 AND [DepthFT] <= 2 THEN 1.31
			WHEN [DepthFT] >=2 AND [DepthFT] <= 3 THEN 1.27
			WHEN [DepthFT] >=3 AND [DepthFT] <= 4 THEN 0.76
			WHEN [DepthFT] >=4 AND [DepthFT] <= 6 THEN 0.12
			WHEN [DepthFT] >=6 AND [DepthFT] <= 12 THEN 0.12
			WHEN [DepthFT] >= 12 THEN 0
	  END AS [EXCAV_DEPTH_EXCEED_COVER_MIT_YINTERCEPT]

      ,CASE WHEN AbovegroundLF = '1' THEN .00001
			WHEN [DepthFT] >=0 AND [DepthFT] <= 1 THEN -0.05
			WHEN [DepthFT] >=1 AND [DepthFT] <= 2 THEN -0.36
			WHEN [DepthFT] >=2 AND [DepthFT] <= 3 THEN -0.34
			WHEN [DepthFT] >=3 AND [DepthFT] <= 4 THEN -0.17
			WHEN [DepthFT] >=4 AND [DepthFT] <= 6 THEN -0.01
			WHEN [DepthFT] >=6 AND [DepthFT] <= 12 THEN -0.01
			WHEN [DepthFT] >=12 THEN .00001
	  END AS [EXCAV_DEPTH_EXCEED_COVER_MIT_COEFX]

      ,CASE
	  		WHEN [AbovegroundLF] = '1' THEN .00001
			WHEN [DepthFT] >= 12 THEN .00001
			ELSE [DepthFT]
	  END AS [EXCAV_DEPTH_EXCEED_COVER_MIT]

	  ---FINAL EQUATION DECOMPOSED
	  --
	  --[EXCAV_DEPTH_EXCEED_COVER_MIT_COEFX] * [EXCAV_DEPTH_EXCEED_COVER_MIT] + [EXCAV_DEPTH_EXCEED_COVER_MIT_YINTERCEPT]
	  --
	  	
		--,CASE 

		--  WHEN [VERT_LOC] = 'ABOVEGROUND' THEN .0000000001
		--	WHEN [DepthFT] >=0 AND [DepthFT] <= 1 THEN -0.05*[DepthFT]+1
		--	WHEN [DepthFT] >=1 AND [DepthFT] <= 2 THEN -0.36*[DepthFT]+1.31
		--	WHEN [DepthFT] >=2 AND [DepthFT] <= 3 THEN -0.34*[DepthFT]+1.27
		--	WHEN [DepthFT] >=3 AND [DepthFT] <= 4 THEN -0.17*[DepthFT]+0.76
		--	WHEN [DepthFT] >=4 AND [DepthFT] <= 6 THEN -0.01*[DepthFT]+0.12
		--	WHEN [DepthFT] >=6 AND [DepthFT] <= 12 THEN -0.01*[DepthFT]+0.12
		--	WHEN [DepthFT] >= 12 THEN .0000000001
		--END AS [EXCAV_DEPTH_EXCEED_COVER_E24]

	  --FAIL OF PREV MEAS
	  ,0.190637  AS [FAIL_OF_PREV_MEAS_MIT]

	   --FAIL OF PROT MEAS
	  --,[CoatingType]
   --   ,[CasingMaterialCl]
   --   ,[SleeveTypeCL]
   --   ,[RiverWeightTypeCL]
	  ,CASE 
		WHEN CoatingType = 'POWERCRETE J' THEN 0.2
		WHEN CoatingType = 'POLY_CONC' THEN 0.2
		WHEN CoatingType = 'MASTIC_CONC' THEN 0.2
		WHEN CoatingType = 'FBE-W/CONC' THEN 0.2
		WHEN SleeveTypeCL = 'Composite' THEN 0.2
		WHEN SleeveTypeCL = 'Manufactured' THEN 0.2
		WHEN SleeveTypeCL = 'Type A' THEN 0.2
		WHEN SleeveTypeCL = 'Type B' THEN 0.2
		WHEN RiverWeightTypeCL = 'CONC WT COATING' THEN 0.2
		WHEN CasingMaterialCL = 'CONCRETE' THEN 0.2
		WHEN CasingMaterialCL = 'CORRUGATED METAL' THEN 0.2
		WHEN CasingMaterialCL = 'POLY' THEN 0.2
		WHEN CasingMaterialCL = 'STEEL' THEN 0.2
		ELSE 1 
		END AS [FAIL_OF_PROT_MEAS_MIT]

      ,[AbovegroundLF]
      --,[POF]
      --,[POF_LL]
      --,[POF_SL]
      --,[POF_R]

	  --RESISTANCE
      ,[TPD_RES]
      --,[TPD_FAIL_MODE_SMALLLEAK]
      --,[TPD_FAIL_MODE_LARGELEAK]
      --,[TPD_FAIL_MODE_RUPTURE]

	  --EXPOSURE
      ,[DIG_RATE_EXP]

	  --[FAIL_OF_PROT_MEAS] * [FAIL_OF_PREV_MEAS] * [EXCAV_DEPTH_EXCEED_COVER] AS MITIGATION
	  --[TPD_RES] AS RESISTANCE
  FROM [PI_ODS_DEV].[STG2].[DL_TPD_POF]

  )


  SELECT 
  sum(EndMeasure-BeginMeasure)/5280 as length
  ,sum((FAIL_OF_PREV_MEAS_MIT * FAIL_OF_PROT_MEAS_MIT * (EXCAV_DEPTH_EXCEED_COVER_MIT_COEFX*EXCAV_DEPTH_EXCEED_COVER_MIT+EXCAV_DEPTH_EXCEED_COVER_MIT_YINTERCEPT))*DIG_RATE_EXP   * (EndMeasure-BeginMeasure)/5280) as impacts
  ,sum(TPD_RES * (FAIL_OF_PREV_MEAS_MIT * FAIL_OF_PROT_MEAS_MIT * (EXCAV_DEPTH_EXCEED_COVER_MIT_COEFX*EXCAV_DEPTH_EXCEED_COVER_MIT+EXCAV_DEPTH_EXCEED_COVER_MIT_YINTERCEPT))*DIG_RATE_EXP   * (EndMeasure-BeginMeasure)/5280)
  FROM T1




