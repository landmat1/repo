USE [PI_QRA_DEV]
GO
/****** Object:  Table [STG1].[INTEGRITY_CISINSPECTION]    Script Date: 11/23/2021 11:24:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[INTEGRITY_CISINSPECTION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[Description] [nvarchar](4000) NULL,
	[Comments] [nvarchar](4000) NULL,
	[AssessmentEventID] [uniqueidentifier] NULL,
	[SourceFile] [nvarchar](255) NULL,
	[BeginDate] [date] NULL,
	[EndDate] [date] NULL,
	[AlignmentDate] [date] NULL,
	[InspectionBeginDate] [date] NULL,
	[InspectionEndDate] [date] NULL,
	[InspectionTypeCL] [nvarchar](18) NULL,
	[CISConnectionType] [nvarchar](255) NULL,
	[BeginLatitude] [numeric](38,8) NULL,
	[BeginLongitude] [numeric](38,8) NULL,
	[EndLatitude] [numeric](38,8) NULL,
	[EndLongitude] [numeric](38,8) NULL,
	[IntCycleOn] [numeric](15,4) NULL,
	[IntCycleOff] [numeric](15,4) NULL,
	[DataLoadedLF] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1].[INTEGRITY_CISINSPECTION] ADD  CONSTRAINT [DF_FAILED_INTEGRITY_CISINSPECTION]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[INTEGRITY_CISINSPECTION] ADD  CONSTRAINT [DF_PULLED_INTEGRITY_CISINSPECTION]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_INTEGRITY_CISINSPECTION]    Script Date: 11/23/2021 11:24:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--IF OBJECT_ID('[STG1].[TRG_TRACK_HIST_INTEGRITY_CISINSPECTION]') IS NOT NULL DROP TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_CISINSPECTION]

CREATE TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_CISINSPECTION] ON [STG1].[INTEGRITY_CISINSPECTION]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISINSPECTION] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,Comments,AssessmentEventID,SourceFile,BeginDate,EndDate,AlignmentDate,InspectionBeginDate,InspectionEndDate,InspectionTypeCL,CISConnectionType,BeginLatitude,BeginLongitude,EndLatitude,EndLongitude,IntCycleOn,IntCycleOff,DataLoadedLF
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'INTEGRITY_CISINSPECTION'
	  ,ID,FAILED,PULLED,EventID,Description,Comments,AssessmentEventID,SourceFile,BeginDate,EndDate,AlignmentDate,InspectionBeginDate,InspectionEndDate,InspectionTypeCL,CISConnectionType,BeginLatitude,BeginLongitude,EndLatitude,EndLongitude,IntCycleOn,IntCycleOff,DataLoadedLF
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISINSPECTION] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,Comments,AssessmentEventID,SourceFile,BeginDate,EndDate,AlignmentDate,InspectionBeginDate,InspectionEndDate,InspectionTypeCL,CISConnectionType,BeginLatitude,BeginLongitude,EndLatitude,EndLongitude,IntCycleOn,IntCycleOff,DataLoadedLF
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'INTEGRITY_CISINSPECTION'
	  ,ID,FAILED,PULLED,EventID,Description,Comments,AssessmentEventID,SourceFile,BeginDate,EndDate,AlignmentDate,InspectionBeginDate,InspectionEndDate,InspectionTypeCL,CISConnectionType,BeginLatitude,BeginLongitude,EndLatitude,EndLongitude,IntCycleOn,IntCycleOff,DataLoadedLF
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISINSPECTION] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,Comments,AssessmentEventID,SourceFile,BeginDate,EndDate,AlignmentDate,InspectionBeginDate,InspectionEndDate,InspectionTypeCL,CISConnectionType,BeginLatitude,BeginLongitude,EndLatitude,EndLongitude,IntCycleOn,IntCycleOff,DataLoadedLF
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'INTEGRITY_CISINSPECTION'
	  ,ID,FAILED,PULLED,EventID,Description,Comments,AssessmentEventID,SourceFile,BeginDate,EndDate,AlignmentDate,InspectionBeginDate,InspectionEndDate,InspectionTypeCL,CISConnectionType,BeginLatitude,BeginLongitude,EndLatitude,EndLongitude,IntCycleOn,IntCycleOff,DataLoadedLF
	from deleted
    RETURN;
  END
END;



GO
ALTER TABLE [STG1].[INTEGRITY_CISINSPECTION] ENABLE TRIGGER [TRG_TRACK_HIST_INTEGRITY_CISINSPECTION]
GO
