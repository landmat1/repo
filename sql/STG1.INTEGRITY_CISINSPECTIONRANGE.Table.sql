USE [PI_QRA_DEV]
GO
/****** Object:  Table [STG1].[INTEGRITY_CISINSPECTIONRANGE]    Script Date: 11/22/2021 3:32:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[INTEGRITY_CISINSPECTIONRANGE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[CreatedDate] [date] NULL,
	[Description] [nvarchar](4000) NULL,
	[EffectiveFromDate] [date] NULL,
	[EffectiveToDate] [date] NULL,
	[LastModified] [date] NULL,
	[Comments] [nvarchar](4000) NULL,
	[RouteEventID] [uniqueidentifier] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[CISInspectionEventID] [uniqueidentifier] NULL,
	[CISSurveyTypeCL] [nvarchar](18) NULL,
	[CISVendorID] [nvarchar](255) NULL,
	[CISEquipment] [nvarchar](255) NULL,
	[GEOM] [geometry] NULL,
	[COMP_RM_GEOM]  AS ([geometry]::STLineFromText(((('LINESTRING ('+CONVERT([nvarchar],[BeginMeasure]))+' 0, ')+CONVERT([nvarchar],[EndMeasure]))+' 0)',(0))),
	[COMP_RM_SLOPE]  AS (case when [BeginMeasure]<[EndMeasure] then (1) when [BeginMeasure]=[EndMeasure] then (0) when [BeginMeasure]>[EndMeasure] then (-1)  end),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [STG1].[INTEGRITY_CISINSPECTIONRANGE] ADD  CONSTRAINT [DF_FAILED_INTEGRITY_CISINSPECTIONRANGE]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[INTEGRITY_CISINSPECTIONRANGE] ADD  CONSTRAINT [DF_PULLED_INTEGRITY_CISINSPECTIONRANGE]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_INTEGRITY_CISINSPECTIONRANGE]    Script Date: 11/22/2021 3:32:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--IF OBJECT_ID('[STG1].[TRG_TRACK_HIST_INTEGRITY_CISINSPECTIONRANGE]') IS NOT NULL DROP TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_CISINSPECTIONRANGE]

CREATE TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_CISINSPECTIONRANGE] ON [STG1].[INTEGRITY_CISINSPECTIONRANGE]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISINSPECTIONRANGE] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CreatedDate,Description,EffectiveFromDate,EffectiveToDate,LastModified,Comments,RouteEventID,BeginMeasure,EndMeasure,CISInspectionEventID,CISSurveyTypeCL,CISVendorID,CISEquipment,GEOM
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'INTEGRITY_CISINSPECTIONRANGE'
	  ,ID,FAILED,PULLED,EventID,CreatedDate,Description,EffectiveFromDate,EffectiveToDate,LastModified,Comments,RouteEventID,BeginMeasure,EndMeasure,CISInspectionEventID,CISSurveyTypeCL,CISVendorID,CISEquipment,GEOM
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISINSPECTIONRANGE] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CreatedDate,Description,EffectiveFromDate,EffectiveToDate,LastModified,Comments,RouteEventID,BeginMeasure,EndMeasure,CISInspectionEventID,CISSurveyTypeCL,CISVendorID,CISEquipment,GEOM
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'INTEGRITY_CISINSPECTIONRANGE'
	  ,ID,FAILED,PULLED,EventID,CreatedDate,Description,EffectiveFromDate,EffectiveToDate,LastModified,Comments,RouteEventID,BeginMeasure,EndMeasure,CISInspectionEventID,CISSurveyTypeCL,CISVendorID,CISEquipment,GEOM
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISINSPECTIONRANGE] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CreatedDate,Description,EffectiveFromDate,EffectiveToDate,LastModified,Comments,RouteEventID,BeginMeasure,EndMeasure,CISInspectionEventID,CISSurveyTypeCL,CISVendorID,CISEquipment,GEOM
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'INTEGRITY_CISINSPECTIONRANGE'
	  ,ID,FAILED,PULLED,EventID,CreatedDate,Description,EffectiveFromDate,EffectiveToDate,LastModified,Comments,RouteEventID,BeginMeasure,EndMeasure,CISInspectionEventID,CISSurveyTypeCL,CISVendorID,CISEquipment,GEOM
	from deleted
    RETURN;
  END
END;




GO
ALTER TABLE [STG1].[INTEGRITY_CISINSPECTIONRANGE] ENABLE TRIGGER [TRG_TRACK_HIST_INTEGRITY_CISINSPECTIONRANGE]
GO
