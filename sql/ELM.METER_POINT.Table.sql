USE [PI_DW_DEV]
GO
/****** Object:  Table [ELM].[METER_POINT]    Script Date: 11/4/2021 9:12:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ELM].[METER_POINT](
	[KEY] [int] IDENTITY(1,1) NOT NULL,
	[CreatedTimestamp] [datetime2](7) NULL,
	[RowEffectiveDate] [date] NULL,
	[RowExpirationDate] [date] NULL,
	[RowCurrentIndicator] [bit] NULL,
	[MD5] [nvarchar](32) NOT NULL,
	[Pin_Number] [int] NOT NULL,
	[MTR_PT_Status] [nvarchar](10) NULL,
	[KMI_Operator_Code1] [nvarchar](5) NULL,
	[KMI_Operator_Name1] [nvarchar](50) NULL,
	[KMI_Operator_Code2] [nvarchar](5) NULL,
	[KMI_Operator_Name2] [nvarchar](50) NULL,
	[MeterName] [nvarchar](100) NULL,
	[MeterDescription] [nvarchar](100) NULL,
	[PointTypeCode] [nvarchar](10) NULL,
	[PointTypeDescription] [nvarchar](100) NULL,
	[FirstDeliveryDate] [date] NULL,
	[CompanyName1] [nvarchar](100) NULL,
	[CompanyName2] [nvarchar](100) NULL,
	[Comment] [nvarchar](max) NULL,
	[PointMaxAllowableOperatingPressure] [int] NULL,
	[Latitude] [numeric](12, 8) NULL,
	[Longitude] [numeric](12, 8) NULL,
PRIMARY KEY CLUSTERED 
(
	[KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [ELM].[METER_POINT] ADD  CONSTRAINT [DF_CREATEDTIMESTAMP_ELM_METER_POINT]  DEFAULT (getdate()) FOR [CreatedTimestamp]
GO
