USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG2].[DL_PIPE_BODY_COR_CALCS]    Script Date: 12/1/2021 6:57:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_PIPE_BODY_COR_CALCS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PIPE_BODY_ID] [int] NULL,
	[CALC_MODE] [nvarchar](15) NULL,
	[OutsideDiameter] [numeric](8, 4) NULL,
	[NominalWallThickness] [numeric](7, 4) NULL,
	[SMYS] [int] NULL,
	[InstallationDate] [datetime2](7) NULL,
	[JOINT_FACTOR] [numeric](2, 1) NULL,
	[DESIGN_PRESSURE_PSIG] [numeric](8, 2) NULL,
	[MAOP] [int] NULL,
	[PIPE_AGE] [numeric](10, 2) NULL,
	[YEARS_SINCE_LAST_ILI] [numeric](10, 2) NULL,
	[YEARS_INSTALL_TO_LAST_PT] [numeric](12, 2) NULL,
	[YEARS_LAST_PT_TO_PRESENT] [numeric](12, 2) NULL,
	[REPAIR_STATUS] [nvarchar](20) NULL,
	[AnomalyType] [nvarchar](50) NULL,
	[Max_or_PeakDepthGrowthRate_INT] [numeric](10, 8) NULL,
	[Max_or_PeakDepthGrowthRate_EXT] [numeric](10, 8) NULL,
	[DefectDepth] [numeric](7, 6) NULL,
	[DefectLength] [numeric](10, 6) NULL,
	[ClockPosition_Degree_Delta] [numeric](5, 2) NULL,
	[MIN_HIGH_POINT_TEST_PRESSURE] [int] NULL,
	[DEFECT_DEPTH_AT_TEST] [numeric](12, 10) NULL,
	[DEFECT_LENGTH_AT_TEST] [numeric](13, 10) NULL,
	[MODELED_COR_IPY] [numeric](17, 15) NULL,
	[LEN_DT_AT_TEST] [numeric](16, 10) NULL,
	[FOLIAS_FACTOR_AT_TEST] [numeric](15, 10) NULL,
	[MB31G_AT_TEST] [numeric](20, 10) NULL,
	[DESIGN_PRESSURE_PSIG_AT_TEST] [numeric](12, 2) NULL,
	[DESIGN_PRESSURE_PSIG_TEST_ADJ] [numeric](12, 2) NULL,
	[DESIGN_PRESSURE_ADJ_YEARS_PAST] [int] NULL,
	[DEFECT_DEPTH_AT_TEST_ADJ] [numeric](12, 10) NULL,
	[DEFECT_LENGTH_AT_TEST_ADJ] [numeric](13, 10) NULL,
	[MODELED_COR_IPY_AT_TEST_ADJ] [numeric](17, 15) NULL,
	[ILI_COR_IPY] [numeric](17, 15) NULL,
	[COR_IPY] [numeric](17, 15) NULL,
	[DEFECT_DEPTH_CURRENT] [numeric](12, 10) NULL,
	[RATIO_DEFECT_DEPTH_TO_LENGTH] [numeric](20, 10) NULL,
	[DEFECT_LENGTH_CURRENT] [numeric](13, 10) NULL,
	[LEN_DT] [numeric](16, 10) NULL,
	[FOLIAS_FACTOR] [numeric](15, 10) NULL,
	[MB31G] [numeric](20, 10) NULL,
	[DESIGN_PRESSURE_PSIG_CURRENT] [numeric](12, 2) NULL,
	[TTF] [int] NULL,
	[DESIGN_PRESSURE_PSIG_FUTURE] [numeric](12, 2) NULL,
	[DESIGN_PRESSURE_PSIG_50YR] [numeric](12, 2) NULL,
	[DESIGN_PRESSURE_PSIG_100YR] [numeric](12, 2) NULL,
	[DESIGN_PRESSURE_PSIG_150YR] [numeric](12, 2) NULL,
	[DESIGN_PRESSURE_MAOP_ADJ_YEARS_PAST] [int] NULL,
	[DESIGN_PRESSURE_PSIG_MAOP_ADJ] [numeric](12, 2) NULL,
	[DEFECT_DEPTH_MAOP_ADJ] [numeric](12, 10) NULL,
	[DEFECT_LENGTH_MAOP_ADJ] [numeric](13, 10) NULL,
	[TIME_TO_LEAK] [numeric](12, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
