USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG2].[DL_STRUCTURE_VALUE_OCCUPANCY_AGG_P_IG_RANGE]    Script Date: 11/2/2021 11:14:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_STRUCTURE_VALUE_OCCUPANCY_AGG_P_IG_RANGE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[FAIL_MODE] [nvarchar](5) NULL,
	[PIR_ID] [int] NULL,
	[P_IGNITION] [numeric](8, 5) NULL,
	[WEIGHTED_VALUE] [numeric](20, 2) NULL,
	[WEIGHTED_OCCUPANCY] [numeric](20, 2) NULL,
	[OCCUPANCY_VALUE] [numeric](20, 2) NULL,
	[TOTAL_VALUE] [numeric](20, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
