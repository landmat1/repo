USE [PI_QRA_DEV]
GO
/****** Object:  Table [UTIL].[LU_GAS_COMPOSITION_SEVERITY_GROUPING]    Script Date: 11/4/2021 4:40:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UTIL].[LU_GAS_COMPOSITION_SEVERITY_GROUPING](
	[UUID] [uniqueidentifier] NOT NULL,
	[GROUPING_ID] [int] NULL,
	[attr_cd] [nvarchar](8) NULL,
	[attr_unit] [nvarchar](100) NULL,
	[MIN_VALUE] [numeric](16, 4) NULL,
	[MIN_VALUE_TYPE] [nvarchar](10) NULL,
	[MAX_VALUE] [numeric](16, 4) NULL,
	[MAX_VALUE_TYPE] [nvarchar](10) NULL,
	[SUSCEPTIBILITY_PROB] [numeric](3, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[UUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
