USE [PI_ODS_DEV]
GO
/****** Object:  Table [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS]    Script Date: 11/2/2021 10:54:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CHG_DATE] [datetime] NOT NULL,
	[CHG_TYPE] [varchar](20) NOT NULL,
	[CHG_BY] [nvarchar](256) NOT NULL,
	[APP_NAME] [nvarchar](128) NOT NULL,
	[HOST_NAME] [nvarchar](128) NOT NULL,
	[SCHEMA_NAME] [sysname] NOT NULL,
	[OBJECT_NAME] [sysname] NOT NULL,
	[PARENT_ID] [int] NOT NULL,
	[E21] [numeric](6, 5) NULL,
	[E24] [numeric](6, 5) NULL,
	[E33] [numeric](6, 5) NULL,
	[E34] [numeric](6, 5) NULL,
	[E41] [numeric](6, 5) NULL,
	[E42] [numeric](6, 5) NULL,
	[E43] [numeric](6, 5) NULL,
	[E46] [numeric](6, 5) NULL,
	[E51] [numeric](6, 5) NULL,
	[E52] [numeric](6, 5) NULL,
	[E63] [numeric](6, 5) NULL,
	[E64] [numeric](6, 5) NULL,
	[E65] [numeric](6, 5) NULL,
	[E66] [numeric](6, 5) NULL,
	[E67] [numeric](6, 5) NULL,
	[E68] [numeric](6, 5) NULL,
	[E74] [numeric](6, 5) NULL,
	[E75] [numeric](6, 5) NULL,
	[E76] [numeric](6, 5) NULL,
	[E82] [numeric](6, 5) NULL,
	[E83] [numeric](6, 5) NULL,
	[E84] [numeric](6, 5) NULL,
	[E85] [numeric](6, 5) NULL,
	[E86] [numeric](6, 5) NULL,
	[E87] [numeric](6, 5) NULL,
	[E91] [numeric](6, 5) NULL,
	[E92] [numeric](6, 5) NULL,
	[E93] [numeric](6, 5) NULL,
	[E94] [numeric](6, 5) NULL,
	[E81] [numeric](6, 5) NULL,
	[E71] [numeric](6, 5) NULL,
	[E72] [numeric](6, 5) NULL,
	[E73] [numeric](6, 5) NULL,
	[E61] [numeric](6, 5) NULL,
	[E62] [numeric](6, 5) NULL,
	[E53] [numeric](6, 5) NULL,
	[E54] [numeric](6, 5) NULL,
	[E55] [numeric](6, 5) NULL,
	[E44] [numeric](6, 5) NULL,
	[E45] [numeric](6, 5) NULL,
	[E31] [numeric](6, 5) NULL,
	[E32] [numeric](6, 5) NULL,
	[E22] [numeric](6, 5) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS] ADD  CONSTRAINT [DF_HIST_CHG_DATE_LU_TPD_FAULT_TREE_DEFAULTS]  DEFAULT (getdate()) FOR [CHG_DATE]
GO
ALTER TABLE [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS] ADD  CONSTRAINT [DF_HIST_CHG_TYPE_LU_TPD_FAULT_TREE_DEFAULTS]  DEFAULT ('') FOR [CHG_TYPE]
GO
ALTER TABLE [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS] ADD  CONSTRAINT [DF_HIST_CHG_BY_LU_TPD_FAULT_TREE_DEFAULTS]  DEFAULT (coalesce(suser_sname(),'?')) FOR [CHG_BY]
GO
ALTER TABLE [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS] ADD  CONSTRAINT [DF_HIST_APP_NAME_LU_TPD_FAULT_TREE_DEFAULTS]  DEFAULT (coalesce(app_name(),'?')) FOR [APP_NAME]
GO
ALTER TABLE [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS] ADD  CONSTRAINT [DF_HIST_HOST_NAME_LU_TPD_FAULT_TREE_DEFAULTS]  DEFAULT (coalesce(host_name(),'?')) FOR [HOST_NAME]
GO
