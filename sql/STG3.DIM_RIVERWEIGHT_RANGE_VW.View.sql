USE [PI_ODS_DEV]
GO
/****** Object:  View [STG3].[DIM_RIVERWEIGHT_RANGE_VW]    Script Date: 11/2/2021 10:54:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [STG3].[DIM_RIVERWEIGHT_RANGE_VW] AS

SELECT
	b.[ID],
	b.[ROUTE_ID],
	b.[BeginMeasure],
	b.[EndMeasure],
    a.[KEY],
	a.[RiverWeightType],
	a.[RiverWeightType_BASIS]
FROM [STG3].[DIM_RIVERWEIGHT] a
INNER JOIN [STG3].[DIM_RIVERWEIGHT_RANGE] b on b.[RIVERWEIGHT_KEY] = a.[KEY]

GO
