USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG2].[DL_IC_MITIGATION_PROBABILITY]    Script Date: 11/8/2021 7:51:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_IC_MITIGATION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[P_CoatingFailure] [numeric](3, 2) NULL,
	[P_ChemFailure] [numeric](3, 2) NULL,
	[P_PigFailure] [numeric](3, 2) NULL,
	[P_MitigationFailure] [numeric](6, 5) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
