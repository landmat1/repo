USE [PI_ODS_DEV]
GO
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'f21f77dd-d2b4-4ebe-a1cf-03cc17a83d7b', 12651, N'Webb Duval', N'COP WEBB-DUVAL')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'e1bdde8b-6a14-4afc-a0fb-0acc6055f512', 164, N'Copano Pipelines/South TX', N'COPFS SOUTH TX')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'93c7d4fc-d199-4eb0-bc21-1c1eecc0dec3', 157, N'Bear Creek', N'BCSC')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'2bd51dea-def4-46aa-aede-1dc2bb1a8584', 3705, N'SNG', N'SNG')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'ae1df58c-4742-4ce0-a7db-1e59233fe36c', 26089, N'Tejas', N'TEJAS')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'ea6005fd-fcdd-454f-a062-1f4024e847fb', 7044, N'GLNG', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'069382ef-c902-4a65-aef6-291e4458f2f5', 19832, N'Hiland', N'HILAND PARTNERS')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'02f21f3d-51ae-4616-accc-32603a42ef44', 549, N'BANHUB', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'7253ff0b-d645-4224-9880-3489a9f250ad', 1279, N'EPNG', N'EPNG')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'aae7f80c-a2e6-40b9-9832-355b8f133ff0', 2140, N'Copano Field Services/South TX', N'COPFS SOUTH TX')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'b10f6a67-f030-405e-9d1b-37feeff7fffb', 3327, N'Copano Pipelines/Upper Gulf Coast', N'COPPL UPPERGULF')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'eaa7be46-7a22-4f05-8359-399c377d30fa', 1030, N'Horizon', N'NGPL')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'84958362-42f6-4d73-8486-4784bcbf54b0', 4798, N'Copano Field Services/North TX', N'COPPL NORTH TX')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'3addea3b-2ffc-4f70-a486-4d77178e2621', 2982, N'FUGG (Fort Union Gas)', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'2e951e22-63ac-4df3-acba-4dd415ebadcd', 530, N'KM Treating', N'KMTLP')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'1d243a66-c195-41e7-941b-514e05d4a126', 5394, N'Ruby', N'CIG')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'3f991437-770e-4d85-90ae-5190f89a4c7d', 8008, N'Keystone', N'KMKGS')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'280e07e2-f020-4bb8-a207-5531bfeb735a', 171, N'ELC  Elba Liquefaction Company', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'bbd75e89-b7ea-4280-88c1-55f556aeed69', 6672, N'Copano Field Services/Upper Gulf Coast', N'COPPL UPPERGULF')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'e84eebcd-a6fb-49cf-9216-5ef61ce1ab47', 630, N'Altamont', N'ALT')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'e8bef8be-a2a0-4f74-a92a-5f7349098b56', 503, N'KMIP', N'NGPL')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'34334c70-21b6-4159-937b-611b9c2d740c', 552, N'KMLP', N'KMLP')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'c543cc4b-7792-480f-8986-66b01b65e18e', 2140, N'Copano Field Services/South TX', N'COPPL SOUTH TX')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'64efd8e6-1dc0-445e-bb84-66e3c26d1065', 177, N'North Dention P/L', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'4086d066-da47-40a9-938a-6901156b0db4', 3369, N'Border', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'fe485b56-0972-4b30-b4f6-72f37fa58975', 158, N'Young Storage', N'CIG')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'671ce7c4-62c9-431d-acab-74219c0938e5', 5143, N'Eagle Ford Gathering', N'EAGLE FORD GATH')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'5a9d34ac-0365-4e2d-bbf1-7634bb698538', 168, N'Sierrita', N'SIERRITA')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'87c2271d-1afb-4cbe-83d7-76a17dc123e5', 539, N'KMNTP', N'KMNTP')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'781701a1-c249-4dc4-be79-78b96072c8a1', 36, N'KMTP', N'KMTP')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'bc8a144d-0dd5-4018-871d-7a056178ddd9', 4669, N'WIC', N'CIG')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'aca2e1ff-8c55-4b61-b205-7b70185d7d94', 3327, N'Copano Pipelines/Upper Gulf Coast', N'COPFS UPPERGULF')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'b3723545-af9a-4358-a715-7ced85ff7521', 957, N'PHP - Permian Highway', N'KMTP')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'45024d59-4a4f-4000-8c9c-8a5d8b086154', 601, N'Camino Real', N'CAMINO')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'0973f483-81c4-42ea-bb0a-8a622b710685', 164, N'Copano Pipelines/South TX', N'COPPL SOUTH TX')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'3cd9bd94-f837-4f67-9ddf-92afa3bdceaf', 4052, N'TGP', N'TGP')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'1640c97a-a2ea-447c-b529-9b3ec6cc4e24', 3324, N'Copano Processing', N'COPFS SOUTH TX')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'ea774c52-7b3c-4904-b5fb-a0771073596b', 165, N'Southern Dome', N'COP SCISSORTAIL')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'74aca387-de9c-4b17-99ab-ac812584d5d3', 4706, N'MEP', N'MEP')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'34701f71-159b-4cfd-b65b-b0d9058decd7', 3324, N'Copano Processing', N'COPPL SOUTH TX')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'308063e8-0956-46ab-b0d0-b1b5f28c3970', 156, N'Elba Express', N'SNG')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'15226d60-c4aa-4d77-99c9-b6b063126efc', 1, N'NGPL', N'NGPL')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'b9b50bb0-2491-40bd-88bd-bc7afe725ec7', 3940, N'Scissortail Energy', N'COP SCISSORTAIL')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'745ec9d8-dd00-48d1-b897-be033ed247cd', 167, N'Cimmarron Gathering', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'7026d9f3-20f7-448e-8bbc-c6901f3237f4', 4121, N'Chey Plains', N'CIG')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'f50a26c5-b011-41ee-be9e-c73560a8f538', 165, N'Southern Dome', N'COP SOUTH DOME')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'db65c1db-4d86-4d63-b74d-c8f2a38e42f4', 959, N'959 - GCX', N'KMTP')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'5f2dfec7-0f42-4e27-a7f0-c9ead739b6ba', 6672, N'Copano Field Services/Upper Gulf Coast', N'COPFS UPPERGULF')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'1ca55dad-e847-44c0-990c-d38e43fada0c', 4798, N'Copano Field Services/North TX', N'COPFS NORTH TX')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'ea2d8f27-9ffc-4fc9-8537-dcacccc607da', 19735, N'Mojave', N'MPC')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'6734a0de-b366-441a-b99c-e06920955043', 155, N'SLNG', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'e65c4b06-1c14-4399-afaf-ea1207ff5b53', 561, N'KHFS', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'f9efb999-e252-4f45-b3ec-ecf2b479ac95', 19, N'Transco', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'cdc17f55-406d-44e5-9cec-edea2673fc53', 166, N'Harrah Midstream', NULL)
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'48779284-ed16-4ca1-89dd-f132adbddf03', 117, N'KMMX', N'KMM')
INSERT [UTIL].[LU_ELM_COMPANY_TO_PODS_OWNER_OPERATOR] ([UUID], [acctg_co_id], [co_nickname], [OperatorCL]) VALUES (N'dde185c5-1f57-40e4-bba8-f97e8abf4a53', 15038, N'CIG', N'CIG')
GO
