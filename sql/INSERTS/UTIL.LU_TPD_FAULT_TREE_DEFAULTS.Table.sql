USE [PI_ODS_DEV]
GO
/****** Object:  Table [UTIL].[LU_TPD_FAULT_TREE_DEFAULTS]    Script Date: 7/27/2021 10:57:58 AM ******/

SET IDENTITY_INSERT [UTIL].[LU_TPD_FAULT_TREE_DEFAULTS] ON 
GO
INSERT [UTIL].[LU_TPD_FAULT_TREE_DEFAULTS]
([ID], [E21], [E24], [E33], [E34], [E41], [E42], [E43], [E46], [E51], [E52], [E63], [E64], [E65], [E66], [E67], [E68], [E74], [E75], [E76], [E82], [E83], [E84], [E85], [E86], [E87], [E91], [E92], [E93], [E94]) 
VALUES (1, CAST(0.09656 AS Numeric(6, 5)), CAST(0.25000 AS Numeric(6, 5)), CAST(1.00000 AS Numeric(6, 5)), CAST(1.00000 AS Numeric(6, 5)), CAST(0.08000 AS Numeric(6, 5)), CAST(0.02000 AS Numeric(6, 5)), CAST(0.09000 AS Numeric(6, 5)), CAST(0.50000 AS Numeric(6, 5)), CAST(0.05000 AS Numeric(6, 5)), CAST(1.00000 AS Numeric(6, 5)), CAST(0.00010 AS Numeric(6, 5)), CAST(0.00000 AS Numeric(6, 5)), CAST(0.00010 AS Numeric(6, 5)), CAST(0.02000 AS Numeric(6, 5)), CAST(0.01000 AS Numeric(6, 5)), CAST(0.05000 AS Numeric(6, 5)), CAST(0.95000 AS Numeric(6, 5)), CAST(0.97000 AS Numeric(6, 5)), CAST(1.00000 AS Numeric(6, 5)), CAST(0.17000 AS Numeric(6, 5)), CAST(0.17000 AS Numeric(6, 5)), CAST(0.21000 AS Numeric(6, 5)), CAST(0.15000 AS Numeric(6, 5)), CAST(0.10000 AS Numeric(6, 5)), CAST(0.04000 AS Numeric(6, 5)), CAST(0.10000 AS Numeric(6, 5)), CAST(0.02000 AS Numeric(6, 5)), CAST(0.10000 AS Numeric(6, 5)), CAST(0.14000 AS Numeric(6, 5)))
GO
SET IDENTITY_INSERT [UTIL].[LU_TPD_FAULT_TREE_DEFAULTS] OFF
GO


