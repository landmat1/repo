USE [PI_QRA_DEV]
GO
INSERT [UTIL].[LU_CHEMICAL_PROPERTIES] ([UUID], [ChemicalType], [P_ChemFailure]) VALUES (N'564b11a2-013f-429f-bc09-08ca71b10e7a', N'Inhibitor', CAST(0.1 AS Numeric(5, 1)))
INSERT [UTIL].[LU_CHEMICAL_PROPERTIES] ([UUID], [ChemicalType], [P_ChemFailure]) VALUES (N'ee5f0091-23a4-4adc-87e6-15b1a6268c17', NULL, CAST(0.5 AS Numeric(5, 1)))
INSERT [UTIL].[LU_CHEMICAL_PROPERTIES] ([UUID], [ChemicalType], [P_ChemFailure]) VALUES (N'44a5ad2b-8116-46b9-b488-1ff48b6a46e1', N'Diesel', CAST(0.8 AS Numeric(5, 1)))
INSERT [UTIL].[LU_CHEMICAL_PROPERTIES] ([UUID], [ChemicalType], [P_ChemFailure]) VALUES (N'947cbe9e-f450-4c21-bc58-5414d02afa37', N'Biocide', CAST(0.1 AS Numeric(5, 1)))
INSERT [UTIL].[LU_CHEMICAL_PROPERTIES] ([UUID], [ChemicalType], [P_ChemFailure]) VALUES (N'45e62c6f-c898-441e-b18d-7afa2fc6a4f4', N'Acid', CAST(1.0 AS Numeric(5, 1)))
INSERT [UTIL].[LU_CHEMICAL_PROPERTIES] ([UUID], [ChemicalType], [P_ChemFailure]) VALUES (N'926af15d-bfc2-4d00-8288-acc29d018877', N'Methanol', CAST(0.8 AS Numeric(5, 1)))
INSERT [UTIL].[LU_CHEMICAL_PROPERTIES] ([UUID], [ChemicalType], [P_ChemFailure]) VALUES (N'0294766c-9657-4154-9056-bcd116afdd2d', N'Cleanser', CAST(0.5 AS Numeric(5, 1)))
INSERT [UTIL].[LU_CHEMICAL_PROPERTIES] ([UUID], [ChemicalType], [P_ChemFailure]) VALUES (N'4e027e0e-bfff-4b62-a6cc-e03a62e39331', N'Imulsn Brk', CAST(1.0 AS Numeric(5, 1)))
GO
