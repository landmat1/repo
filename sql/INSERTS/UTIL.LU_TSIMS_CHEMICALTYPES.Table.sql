USE [PI_QRA_DEV]
GO
INSERT [UTIL].[LU_TSIMS_CHEMICALTYPES] ([UUID], [TSIMS_ChemicalID], [Type], [Inactive]) VALUES (N'82ee2a91-db59-43e1-a933-08fe803092f4', 1, N'Inhibitor', NULL)
INSERT [UTIL].[LU_TSIMS_CHEMICALTYPES] ([UUID], [TSIMS_ChemicalID], [Type], [Inactive]) VALUES (N'75c94c3a-be8c-46bf-b357-87a284ffe83c', 6, N'Acid', 0)
INSERT [UTIL].[LU_TSIMS_CHEMICALTYPES] ([UUID], [TSIMS_ChemicalID], [Type], [Inactive]) VALUES (N'b97c106c-ac33-40a3-9f74-95689315785e', 2, N'Biocide', NULL)
INSERT [UTIL].[LU_TSIMS_CHEMICALTYPES] ([UUID], [TSIMS_ChemicalID], [Type], [Inactive]) VALUES (N'cf506881-0f92-45a5-bd08-c1dcc591c10d', 3, N'Methanol', NULL)
INSERT [UTIL].[LU_TSIMS_CHEMICALTYPES] ([UUID], [TSIMS_ChemicalID], [Type], [Inactive]) VALUES (N'ef606fa0-e007-40f0-bf2c-dcd0c2cfe2eb', 9, N'Diesel', 0)
INSERT [UTIL].[LU_TSIMS_CHEMICALTYPES] ([UUID], [TSIMS_ChemicalID], [Type], [Inactive]) VALUES (N'd789589b-d8cf-47ec-8239-e27e2e35aabd', 4, N'Cleanser', 0)
INSERT [UTIL].[LU_TSIMS_CHEMICALTYPES] ([UUID], [TSIMS_ChemicalID], [Type], [Inactive]) VALUES (N'465f1e2d-b278-4f93-8c64-faae4c868016', 8, N'Imulsn Brk', 0)
GO
