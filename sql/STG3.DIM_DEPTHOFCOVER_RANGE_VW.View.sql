USE [PI_ODS_DEV]
GO
/****** Object:  View [STG3].[DIM_DEPTHOFCOVER_RANGE_VW]    Script Date: 11/2/2021 10:54:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [STG3].[DIM_DEPTHOFCOVER_RANGE_VW] AS

SELECT
	b.[ID],
	b.[ROUTE_ID],
	b.[BeginMeasure],
	b.[EndMeasure],
	a.[KEY],
	a.[DepthFT],
    a.[AbovegroundLF],
    a.[Basis]
FROM [STG3].[DIM_DEPTHOFCOVER] a
INNER JOIN [STG3].[DIM_DEPTHOFCOVER_RANGE] b on b.[DEPTHOFCOVER_KEY] = a.[KEY]

GO
