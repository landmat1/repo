USE [PI_QRA_DEV]
GO
/****** Object:  Table [STG1_HIST].[INTEGRITY_CISDATA]    Script Date: 11/9/2021 7:26:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1_HIST].[INTEGRITY_CISDATA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CHG_DATE] [datetime] NOT NULL,
	[CHG_TYPE] [varchar](20) NOT NULL,
	[CHG_BY] [nvarchar](256) NOT NULL,
	[APP_NAME] [nvarchar](128) NOT NULL,
	[HOST_NAME] [nvarchar](128) NOT NULL,
	[SCHEMA_NAME] [sysname] NOT NULL,
	[OBJECT_NAME] [sysname] NOT NULL,
	[PARENT_ID] [int] NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[CISInspectionRangeEventID] [uniqueidentifier] NULL,
	[RouteEventID] [uniqueidentifier] NULL,
	[Measure] [numeric](15, 2) NULL,
	[InspectionDate] [date] NULL,
	[OffsetDistance] [numeric](7,2) NULL,
	[OffsetDirectionCL] [nvarchar](18) NULL,
	[NearGroundOn] [numeric](7, 3) NULL,
	[NearGroundOff] [numeric](7, 3) NULL,
	[FarGroundOn] [numeric](7, 3) NULL,
	[FarGroundOff] [numeric](7, 3) NULL,
	[PSOn] [numeric](7, 3) NULL,
	[PSOff] [numeric](7, 3) NULL,
	[RLATOn] [numeric](7, 3) NULL,
	[RLATOff] [numeric](7, 3) NULL,
	[LLATOn] [numeric](7, 3) NULL,
	[LLATOff] [numeric](7, 3) NULL,
	[MIROn] [numeric](7, 3) NULL,
	[MIROff] [numeric](7, 3) NULL,
	[MIRDepol] [numeric](7, 3) NULL,
	[CASOn] [numeric](7, 3) NULL,
	[CASOff] [numeric](7, 3) NULL,
	[CASDepol] [numeric](7, 3) NULL,
	[FOROn] [numeric](7, 3) NULL,
	[FOROff] [numeric](7, 3) NULL,
	[FORDepol] [numeric](7, 3) NULL,
	[FarGroundDepol] [numeric](7, 3) NULL,
	[FarGroundAC] [numeric](7, 3) NULL,
	[LLATDepol] [numeric](7, 3) NULL,
	[NearGroundDepol] [numeric](7, 3) NULL,
	[NearGroundAC] [numeric](7, 3) NULL,
	[PSDepol] [numeric](7, 3) NULL,
	[PSAC] [numeric](7, 3) NULL,
	[RlatDepol] [numeric](7, 3) NULL,
	[POINT_X] [numeric](38, 8) NULL,
	[POINT_Y] [numeric](38, 8) NULL,
	[POINT_Z] [numeric](38, 8) NULL,
	[X_Reported] [numeric](38, 8) NULL,
	[Y_Reported] [numeric](38, 8) NULL,
	[Z_Reported] [numeric](38, 8) NULL,
	[GEOM] [geometry] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISDATA] ADD  CONSTRAINT [DF_HIST_CHG_DATE_INTEGRITY_CISDATA]  DEFAULT (getdate()) FOR [CHG_DATE]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISDATA] ADD  CONSTRAINT [DF_HIST_CHG_TYPE_INTEGRITY_CISDATA]  DEFAULT ('') FOR [CHG_TYPE]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISDATA] ADD  CONSTRAINT [DF_HIST_CHG_BY_INTEGRITY_CISDATA]  DEFAULT (coalesce(suser_sname(),'?')) FOR [CHG_BY]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISDATA] ADD  CONSTRAINT [DF_HIST_APP_NAME_INTEGRITY_CISDATA]  DEFAULT (coalesce(app_name(),'?')) FOR [APP_NAME]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISDATA] ADD  CONSTRAINT [DF_HIST_HOST_NAME_INTEGRITY_CISDATA]  DEFAULT (coalesce(host_name(),'?')) FOR [HOST_NAME]
GO
