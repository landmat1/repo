USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY]    Script Date: 11/16/2021 10:28:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CHG_DATE] [datetime] NOT NULL,
	[CHG_TYPE] [varchar](20) NOT NULL,
	[CHG_BY] [nvarchar](256) NOT NULL,
	[APP_NAME] [nvarchar](128) NOT NULL,
	[HOST_NAME] [nvarchar](128) NOT NULL,
	[SCHEMA_NAME] [sysname] NOT NULL,
	[OBJECT_NAME] [sysname] NOT NULL,
	[PARENT_ID] [int] NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[Description] [nvarchar](4000) NULL,
	[Comments] [nvarchar](4000) NULL,
	[ILIDataEventID] [uniqueidentifier] NULL,
	[ILIDigNumber] [nvarchar](32) NULL,
	[DiscoveryDate] [datetime2](7) NULL,
	[BeginDigOdometer] [numeric](16, 3) NULL,
	[EndDigOdometer] [numeric](16, 3) NULL,
	[BeginDigStation] [numeric](16, 3) NULL,
	[EndDigStation] [numeric](16, 3) NULL,
	[DigLength] [numeric](9, 3) NULL,
	[VarianceRequiredLF] [nvarchar](25) NULL,
	[JustificationSubmittedLF] [nvarchar](25) NULL,
	[VarianceRefNumber] [nvarchar](32) NULL,
	[MOCRefNumber] [nvarchar](32) NULL,
	[PIStatusCL] [nvarchar](18) NULL,
	[ALGPriorityComments] [nvarchar](2000) NULL,
	[RStrengEFFAreaMaxSafePR] [numeric](16, 3) NULL,
	[ActualDF] [numeric](7, 3) NULL,
	[Algorithm] [nvarchar](100) NULL,
	[ResponseCriteria] [nvarchar](1000) NULL,
	[ResponseClass] [nvarchar](100) NULL,
	[DueDate] [date] NULL,
	[AbsoluteOdometer] [numeric](16, 3) NULL,
	[KMAnomalyTypeCL] [nvarchar](100) NULL,
	[ResponseSchedule] [nvarchar](8) NULL,
	[Priority] [nvarchar](50) NULL,
	[ConsequenceClass] [nvarchar](100) NULL,
	[AlternativeResponsePlanLF] [nvarchar](18) NULL,
	[TechnicalJustificationLF] [nvarchar](18) NULL,
	[ReportNumber] [nvarchar](255) NULL,
	[ServiceProviderJobNumber] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY] ADD  CONSTRAINT [DF_HIST_CHG_DATE_INTEGRITY_ACTIONABLEANOMALY]  DEFAULT (getdate()) FOR [CHG_DATE]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY] ADD  CONSTRAINT [DF_HIST_CHG_TYPE_INTEGRITY_ACTIONABLEANOMALY]  DEFAULT ('') FOR [CHG_TYPE]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY] ADD  CONSTRAINT [DF_HIST_CHG_BY_INTEGRITY_ACTIONABLEANOMALY]  DEFAULT (coalesce(suser_sname(),'?')) FOR [CHG_BY]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY] ADD  CONSTRAINT [DF_HIST_APP_NAME_INTEGRITY_ACTIONABLEANOMALY]  DEFAULT (coalesce(app_name(),'?')) FOR [APP_NAME]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY] ADD  CONSTRAINT [DF_HIST_HOST_NAME_INTEGRITY_ACTIONABLEANOMALY]  DEFAULT (coalesce(host_name(),'?')) FOR [HOST_NAME]
GO
