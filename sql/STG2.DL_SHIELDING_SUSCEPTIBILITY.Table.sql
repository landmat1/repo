USE [PI_QRA_DEV]
GO
/****** Object:  Table [STG2].[DL_SHIELDING_SUSCEPTIBILITY]    Script Date: 12/8/2021 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_SHIELDING_SUSCEPTIBILITY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[CoatingType] [nvarchar](50) NULL,
	[P_CoatingShielding] [numeric](6, 5) NULL,
	[CasingShorted] [nvarchar](25) NULL,
	[P_CasingShielding] [numeric](6, 5) NULL,
	[SleeveMaterial] [nvarchar](50) NULL,
	[P_SleeveShielding] [numeric](6, 5) NULL,
	[P_Shielding] [numeric](6, 5) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
