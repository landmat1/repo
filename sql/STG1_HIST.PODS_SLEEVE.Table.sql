USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG1_HIST].[PODS_SLEEVE]    Script Date: 11/2/2021 10:54:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1_HIST].[PODS_SLEEVE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CHG_DATE] [datetime] NOT NULL,
	[CHG_TYPE] [varchar](20) NOT NULL,
	[CHG_BY] [nvarchar](256) NOT NULL,
	[APP_NAME] [nvarchar](128) NOT NULL,
	[HOST_NAME] [nvarchar](128) NOT NULL,
	[SCHEMA_NAME] [sysname] NOT NULL,
	[OBJECT_NAME] [sysname] NOT NULL,
	[PARENT_ID] [int] NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[RouteEventID] [uniqueidentifier] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[Description] [nvarchar](4000) NULL,
	[InstallationDate] [datetime2](7) NULL,
	[PipeGradeCL] [nvarchar](18) NULL,
	[DateManufactured] [datetime2](7) NULL,
	[ManufacturerCL] [nvarchar](50) NULL,
	[MaterialCL] [nvarchar](50) NULL,
	[MillTestPressure] [int] NULL,
	[NominalPressureRating] [int] NULL,
	[NominalWallThicknessCL] [numeric](7, 4) NULL,
	[NominalDiameterCL] [numeric](8, 4) NULL,
	[SpecificationCL] [nvarchar](50) NULL,
	[TypeCL] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1_HIST].[PODS_SLEEVE] ADD  CONSTRAINT [DF_HIST_CHG_DATE_PODS_SLEEVE]  DEFAULT (getdate()) FOR [CHG_DATE]
GO
ALTER TABLE [STG1_HIST].[PODS_SLEEVE] ADD  CONSTRAINT [DF_HIST_CHG_TYPE_PODS_SLEEVE]  DEFAULT ('') FOR [CHG_TYPE]
GO
ALTER TABLE [STG1_HIST].[PODS_SLEEVE] ADD  CONSTRAINT [DF_HIST_CHG_BY_PODS_SLEEVE]  DEFAULT (coalesce(suser_sname(),'?')) FOR [CHG_BY]
GO
ALTER TABLE [STG1_HIST].[PODS_SLEEVE] ADD  CONSTRAINT [DF_HIST_APP_NAME_PODS_SLEEVE]  DEFAULT (coalesce(app_name(),'?')) FOR [APP_NAME]
GO
ALTER TABLE [STG1_HIST].[PODS_SLEEVE] ADD  CONSTRAINT [DF_HIST_HOST_NAME_PODS_SLEEVE]  DEFAULT (coalesce(host_name(),'?')) FOR [HOST_NAME]
GO
