USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG3].[DIM_PIPE_MANUFACTURE]    Script Date: 11/2/2021 10:10:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG3].[DIM_PIPE_MANUFACTURE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedTimestamp] [datetime2](7) NULL,
	[PIPE_MANUFACTURE_KEY] [int] NULL,
	[Material] [nvarchar](50) NULL,
	[Manufacturer] [nvarchar](50) NULL,
	[Specification] [nvarchar](50) NULL,
	[NominalOutsideDiameter] [numeric](8, 4) NULL,
	[InsideDiameter] [numeric](8, 4) NULL,
	[NominalWallThickness] [numeric](7, 4) NULL,
	[DT_Ratio] [numeric](15, 4) NULL,
	[Grade] [nvarchar](50) NULL,
	[SMYS] [int] NULL,
	[SMYS10K] [int] NULL,
	[LongSeam] [nvarchar](50) NULL,
	[MillLocation] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG3].[DIM_PIPE_MANUFACTURE] ADD  CONSTRAINT [DF_CREATEDTIMESTAMP_DIM_PIPE_MANUFACTURE]  DEFAULT (getdate()) FOR [CreatedTimestamp]
GO
