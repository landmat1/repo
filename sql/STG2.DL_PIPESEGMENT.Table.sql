USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG2].[DL_PIPESEGMENT]    Script Date: 11/29/2021 11:27:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_PIPESEGMENT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[Material] [nvarchar](50) NULL,
	[Material_BASIS] [nvarchar](100) NULL,
	[Manufacturer] [nvarchar](50) NULL,
	[Specification] [nvarchar](50) NULL,
	[OutsideDiameter] [numeric](8, 4) NULL,
	[OutsideDiameter_BASIS] [nvarchar](100) NULL,
	[InsideDiameter] [numeric](8, 4) NULL,
	[NominalDiameter] [numeric](8, 4) NULL,
	[NominalWallThickness] [numeric](7, 4) NULL,
	[NominalWallThickness_BASIS] [nvarchar](100) NULL,
	[DT_Ratio] [numeric](15, 4) NULL,
	[Grade] [nvarchar](50) NULL,
	[SMYS] [int] NULL,
	[SMYS_BASIS] [nvarchar](100) NULL,
	[SMYS10K] [int] NULL,
	[LongSeam] [nvarchar](50) NULL,
	[MillLocation] [nvarchar](50) NULL,
	[PipeProperty_BASIS] [nvarchar](500) NULL,
	[InstallationDate] [date] NULL,
	[InstallationDate_BASIS] [nvarchar](500) NULL,
	[PIPE_AGE] [numeric](15, 7) NULL,
	[JOINT_FACTOR] [numeric](2, 1) NULL,
	[DESIGN_PRESSURE_PSIG] [numeric](8, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
