USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG1].[PODS_PIPESEGMENT_STEEL]    Script Date: 11/2/2021 10:10:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[PODS_PIPESEGMENT_STEEL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[RouteEventID] [uniqueidentifier] NULL,
	[InstallationDate] [date] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[GirthWeldTypeCL] [nvarchar](50) NULL,
	[MaterialCL] [nvarchar](50) NULL,
	[OutsideDiameterCL] [numeric](8, 4) NULL,
	[NominalDiameterCL] [numeric](8, 4) NULL,
	[ManufacturerCL] [nvarchar](50) NULL,
	[PipeMillLocationCL] [nvarchar](50) NULL,
	[MillTestPressure] [int] NULL,
	[PipeSpecificationCL] [nvarchar](50) NULL,
	[PipeGradeCL] [nvarchar](50) NULL,
	[SMYSCL] [int] NULL,
	[NominalWallThicknessCL] [numeric](7, 4) NULL,
	[PipeLongSeamCL] [nvarchar](50) NULL,
	[COMP_RM_GEOM]  AS ([geometry]::STLineFromText(((('LINESTRING ('+CONVERT([nvarchar],[BeginMeasure]))+' 0, ')+CONVERT([nvarchar],[EndMeasure]))+' 0)',(0))),
	[COMP_RM_SLOPE]  AS (case when [BeginMeasure]<[EndMeasure] then (1) when [BeginMeasure]=[EndMeasure] then (0) when [BeginMeasure]>[EndMeasure] then (-1)  end),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1].[PODS_PIPESEGMENT_STEEL] ADD  CONSTRAINT [DF_FAILED_PODS_PIPESEGMENT_STEEL]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[PODS_PIPESEGMENT_STEEL] ADD  CONSTRAINT [DF_PULLED_PODS_PIPESEGMENT_STEEL]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_PODS_PIPESEGMENT_STEEL]    Script Date: 11/2/2021 10:10:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--IF OBJECT_ID('[STG1].[TRG_TRACK_HIST_PODS_PIPESEGMENT_STEEL]') IS NOT NULL DROP TRIGGER [STG1].[TRG_TRACK_HIST_PODS_PIPESEGMENT_STEEL]

CREATE TRIGGER [STG1].[TRG_TRACK_HIST_PODS_PIPESEGMENT_STEEL] ON [STG1].[PODS_PIPESEGMENT_STEEL]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[PODS_PIPESEGMENT_STEEL] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,RouteEventID,InstallationDate,BeginMeasure,EndMeasure,GirthWeldTypeCL,MaterialCL,OutsideDiameterCL,NominalDiameterCL,ManufacturerCL,PipeMillLocationCL,MillTestPressure,PipeSpecificationCL,PipeGradeCL,SMYSCL,NominalWallThicknessCL,PipeLongSeamCL
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'PODS_PIPESEGMENT_STEEL'
	  ,ID,FAILED,PULLED,EventID,RouteEventID,InstallationDate,BeginMeasure,EndMeasure,GirthWeldTypeCL,MaterialCL,OutsideDiameterCL,NominalDiameterCL,ManufacturerCL,PipeMillLocationCL,MillTestPressure,PipeSpecificationCL,PipeGradeCL,SMYSCL,NominalWallThicknessCL,PipeLongSeamCL
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[PODS_PIPESEGMENT_STEEL] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,RouteEventID,InstallationDate,BeginMeasure,EndMeasure,GirthWeldTypeCL,MaterialCL,OutsideDiameterCL,NominalDiameterCL,ManufacturerCL,PipeMillLocationCL,MillTestPressure,PipeSpecificationCL,PipeGradeCL,SMYSCL,NominalWallThicknessCL,PipeLongSeamCL
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'PODS_PIPESEGMENT_STEEL'
	  ,ID,FAILED,PULLED,EventID,RouteEventID,InstallationDate,BeginMeasure,EndMeasure,GirthWeldTypeCL,MaterialCL,OutsideDiameterCL,NominalDiameterCL,ManufacturerCL,PipeMillLocationCL,MillTestPressure,PipeSpecificationCL,PipeGradeCL,SMYSCL,NominalWallThicknessCL,PipeLongSeamCL
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[PODS_PIPESEGMENT_STEEL] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,RouteEventID,InstallationDate,BeginMeasure,EndMeasure,GirthWeldTypeCL,MaterialCL,OutsideDiameterCL,NominalDiameterCL,ManufacturerCL,PipeMillLocationCL,MillTestPressure,PipeSpecificationCL,PipeGradeCL,SMYSCL,NominalWallThicknessCL,PipeLongSeamCL
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'PODS_PIPESEGMENT_STEEL'
	  ,ID,FAILED,PULLED,EventID,RouteEventID,InstallationDate,BeginMeasure,EndMeasure,GirthWeldTypeCL,MaterialCL,OutsideDiameterCL,NominalDiameterCL,ManufacturerCL,PipeMillLocationCL,MillTestPressure,PipeSpecificationCL,PipeGradeCL,SMYSCL,NominalWallThicknessCL,PipeLongSeamCL
	from deleted
    RETURN;
  END
END;





GO
ALTER TABLE [STG1].[PODS_PIPESEGMENT_STEEL] ENABLE TRIGGER [TRG_TRACK_HIST_PODS_PIPESEGMENT_STEEL]
GO
