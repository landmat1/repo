USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG1].[INTEGRITY_ILIDATA_ADDITIONAL]    Script Date: 12/2/2021 3:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[INTEGRITY_ILIDATA_ADDITIONAL](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[ILIInspectionRangeEventID] [uniqueidentifier] NULL,
	[MaxGrowthRateInCluster] [numeric](6, 2) NULL,
	[MaxGrowthInClusterComment] [nvarchar](25) NULL,
	[PeakDepthGrowthRate] [numeric](6, 2) NULL,
	[CorrosionGrowthCommCL] [nvarchar](25) NULL,
	[GeoFittingDescrCL] [nvarchar](25) NULL,
	[BendRadius] [nvarchar](25) NULL,
	[BendAngle] [numeric](15, 3) NULL,
	[DSWeldDistance] [numeric](16, 3) NULL,
	[USWeldDistance] [numeric](16, 3) NULL,
	[DistFrDSGW] [numeric](10, 3) NULL,
	[DistFrUSGW] [numeric](10, 3) NULL,
	[PossibleSeamInteractionCL] [nvarchar](25) NULL,
	[WrinkleWLSLF] [nvarchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1].[INTEGRITY_ILIDATA_ADDITIONAL] ADD  CONSTRAINT [DF_FAILED_INTEGRITY_ILIDATA_ADDITIONAL]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[INTEGRITY_ILIDATA_ADDITIONAL] ADD  CONSTRAINT [DF_PULLED_INTEGRITY_ILIDATA_ADDITIONAL]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_INTEGRITY_ILIDATA_ADDITIONAL]    Script Date: 12/2/2021 3:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_ILIDATA_ADDITIONAL] ON [STG1].[INTEGRITY_ILIDATA_ADDITIONAL]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_ILIDATA_ADDITIONAL] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,MaxGrowthRateInCluster,MaxGrowthInClusterComment,PeakDepthGrowthRate,CorrosionGrowthCommCL,GeoFittingDescrCL,BendRadius,BendAngle,USWeldDistance,DSWeldDistance,DistFrDSGW, DistFrUSGW, PossibleSeamInteractionCL,WrinkleWLSLF
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'INTEGRITY_ILIDATA_ADDITIONAL'
	  ,ID,FAILED,PULLED,EventID,MaxGrowthRateInCluster,MaxGrowthInClusterComment,PeakDepthGrowthRate,CorrosionGrowthCommCL,GeoFittingDescrCL,BendRadius,BendAngle,USWeldDistance,DSWeldDistance,DistFrDSGW, DistFrUSGW, PossibleSeamInteractionCL,WrinkleWLSLF
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_ILIDATA_ADDITIONAL] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,MaxGrowthRateInCluster,MaxGrowthInClusterComment,PeakDepthGrowthRate,CorrosionGrowthCommCL,GeoFittingDescrCL,BendRadius,BendAngle,USWeldDistance,DSWeldDistance,DistFrDSGW, DistFrUSGW, PossibleSeamInteractionCL,WrinkleWLSLF
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'INTEGRITY_ILIDATA_ADDITIONAL'
	  ,ID,FAILED,PULLED,EventID,MaxGrowthRateInCluster,MaxGrowthInClusterComment,PeakDepthGrowthRate,CorrosionGrowthCommCL,GeoFittingDescrCL,BendRadius,BendAngle,USWeldDistance,DSWeldDistance,DistFrDSGW, DistFrUSGW, PossibleSeamInteractionCL,WrinkleWLSLF
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_ILIDATA_ADDITIONAL] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,MaxGrowthRateInCluster,MaxGrowthInClusterComment,PeakDepthGrowthRate,CorrosionGrowthCommCL,GeoFittingDescrCL,BendRadius,BendAngle,USWeldDistance,DSWeldDistance,DistFrDSGW, DistFrUSGW, PossibleSeamInteractionCL,WrinkleWLSLF
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'INTEGRITY_ILIDATA_ADDITIONAL'
	  ,ID,FAILED,PULLED,EventID,MaxGrowthRateInCluster,MaxGrowthInClusterComment,PeakDepthGrowthRate,CorrosionGrowthCommCL,GeoFittingDescrCL,BendRadius,BendAngle,USWeldDistance,DSWeldDistance,DistFrDSGW, DistFrUSGW, PossibleSeamInteractionCL,WrinkleWLSLF
	from deleted
    RETURN;
  END
END;





ALTER TABLE [STG1].[INTEGRITY_ILIDATA_ADDITIONAL] ENABLE TRIGGER [TRG_TRACK_HIST_INTEGRITY_ILIDATA_ADDITIONAL]
GO
ALTER TABLE [STG1].[INTEGRITY_ILIDATA_ADDITIONAL] ENABLE TRIGGER [TRG_TRACK_HIST_INTEGRITY_ILIDATA_ADDITIONAL]
GO
