USE [PI_QRA_DEV]
GO
/****** Object:  Table [STG2].[DL_AC_CORRIDOR_RANGE]    Script Date: 12/8/2021 4:11:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_AC_CORRIDOR_RANGE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[CROSSING_COUNT] [int] NULL,
	[Max_AC_Company] [nvarchar](100) NULL,
	[Max_AC_Voltage_KV] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
