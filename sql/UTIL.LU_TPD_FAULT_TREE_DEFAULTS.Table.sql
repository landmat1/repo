USE [PI_ODS_DEV]
GO
/****** Object:  Table [UTIL].[LU_TPD_FAULT_TREE_DEFAULTS]    Script Date: 11/2/2021 10:54:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UTIL].[LU_TPD_FAULT_TREE_DEFAULTS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[E21] [numeric](6, 5) NULL,
	[E24] [numeric](6, 5) NULL,
	[E33] [numeric](6, 5) NULL,
	[E34] [numeric](6, 5) NULL,
	[E41] [numeric](6, 5) NULL,
	[E42] [numeric](6, 5) NULL,
	[E43] [numeric](6, 5) NULL,
	[E46] [numeric](6, 5) NULL,
	[E51] [numeric](6, 5) NULL,
	[E52] [numeric](6, 5) NULL,
	[E63] [numeric](6, 5) NULL,
	[E64] [numeric](6, 5) NULL,
	[E65] [numeric](6, 5) NULL,
	[E66] [numeric](6, 5) NULL,
	[E67] [numeric](6, 5) NULL,
	[E68] [numeric](6, 5) NULL,
	[E74] [numeric](6, 5) NULL,
	[E75] [numeric](6, 5) NULL,
	[E76] [numeric](6, 5) NULL,
	[E82] [numeric](6, 5) NULL,
	[E83] [numeric](6, 5) NULL,
	[E84] [numeric](6, 5) NULL,
	[E85] [numeric](6, 5) NULL,
	[E86] [numeric](6, 5) NULL,
	[E87] [numeric](6, 5) NULL,
	[E91] [numeric](6, 5) NULL,
	[E92] [numeric](6, 5) NULL,
	[E93] [numeric](6, 5) NULL,
	[E94] [numeric](6, 5) NULL,
	[E81]  AS ((1)-((((1)-[E91])*((1)-[E92]))*((1)-[E93]))*((1)-[E94])) PERSISTED,
	[E71]  AS (((1)-((((1)-[E91])*((1)-[E92]))*((1)-[E93]))*((1)-[E94]))*[E82]),
	[E72]  AS ((1)-((1)-[E83])*((1)-[E84])),
	[E73]  AS ((1)-(((1)-[E85])*((1)-[E86]))*((1)-[E87])),
	[E61]  AS ((1)-(((1)-((1)-((((1)-[E91])*((1)-[E92]))*((1)-[E93]))*((1)-[E94]))*[E82])*((1)-((1)-((1)-[E83])*((1)-[E84]))))*((1)-((1)-(((1)-[E85])*((1)-[E86]))*((1)-[E87])))),
	[E62]  AS (([E74]*[E75])*[E76]),
	[E53]  AS (((1)-(((1)-((1)-((((1)-[E91])*((1)-[E92]))*((1)-[E93]))*((1)-[E94]))*[E82])*((1)-((1)-((1)-[E83])*((1)-[E84]))))*((1)-((1)-(((1)-[E85])*((1)-[E86]))*((1)-[E87]))))*(([E74]*[E75])*[E76])),
	[E54]  AS ((1)-(((1)-[E63])*((1)-[E64]))*((1)-[E65])),
	[E55]  AS ((1)-(((1)-[E66])*((1)-[E67]))*((1)-[E68])),
	[E44]  AS ([E51]*[E52]),
	[E45]  AS ((1)-(((1)-((1)-(((1)-((1)-((((1)-[E91])*((1)-[E92]))*((1)-[E93]))*((1)-[E94]))*[E82])*((1)-((1)-((1)-[E83])*((1)-[E84]))))*((1)-((1)-(((1)-[E85])*((1)-[E86]))*((1)-[E87]))))*(([E74]*[E75])*[E76]))*((1)-((1)-(((1)-[E63])*((1)-[E64]))*((1)-[E65]))))*((1)-((1)-(((1)-[E66])*((1)-[E67]))*((1)-[E68])))),
	[E31]  AS ((1)-(((1)-[E41])*((1)-[E42]))*((1)-[E43])),
	[E32]  AS ((([E51]*[E52])*((1)-(((1)-((1)-(((1)-((1)-((((1)-[E91])*((1)-[E92]))*((1)-[E93]))*((1)-[E94]))*[E82])*((1)-((1)-((1)-[E83])*((1)-[E84]))))*((1)-((1)-(((1)-[E85])*((1)-[E86]))*((1)-[E87]))))*(([E74]*[E75])*[E76]))*((1)-((1)-(((1)-[E63])*((1)-[E64]))*((1)-[E65]))))*((1)-((1)-(((1)-[E66])*((1)-[E67]))*((1)-[E68])))))*[E46]),
	[E22]  AS ((1)-((1)-((1)-(((1)-[E41])*((1)-[E42]))*((1)-[E43])))*((1)-(([E51]*[E52])*((1)-(((1)-((1)-(((1)-((1)-((((1)-[E91])*((1)-[E92]))*((1)-[E93]))*((1)-[E94]))*[E82])*((1)-((1)-((1)-[E83])*((1)-[E84]))))*((1)-((1)-(((1)-[E85])*((1)-[E86]))*((1)-[E87]))))*(([E74]*[E75])*[E76]))*((1)-((1)-(((1)-[E63])*((1)-[E64]))*((1)-[E65]))))*((1)-((1)-(((1)-[E66])*((1)-[E67]))*((1)-[E68])))))*[E46])),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Trigger [UTIL].[TRG_TRACK_HIST_LU_TPD_FAULT_TREE_DEFAULTS]    Script Date: 11/2/2021 10:54:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [UTIL].[TRG_TRACK_HIST_LU_TPD_FAULT_TREE_DEFAULTS] ON [UTIL].[LU_TPD_FAULT_TREE_DEFAULTS]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS] (
      [CHG_TYPE],
      [SCHEMA_NAME],
      [OBJECT_NAME],

      [PARENT_ID],
	  [E21] ,
	[E24] ,
	[E33] ,
	[E34] ,
	[E41] ,
	[E42] ,
	[E43] ,
	[E46] ,
	[E51] ,
	[E52] ,
	[E63] ,
	[E64] ,
	[E65] ,
	[E66] ,
	[E67] ,
	[E68] ,
	[E74] ,
	[E75] ,
	[E76] ,
	[E82] ,
	[E83] ,
	[E84] ,
	[E85] ,
	[E86] ,
	[E87] ,
	[E91] ,
	[E92] ,
	[E93] ,
	[E94] ,
	[E81] ,
	[E71] ,
	[E72] ,
	[E73] ,
	[E61] ,
	[E62] ,
	[E53] ,
	[E54] ,
	[E55] ,
	[E44] ,
	[E45] ,
	[E31] ,
	[E32] ,
	[E22] 
       )
    SELECT
       'INSERT'
      ,'UTIL'
      ,'LU_TPD_FAULT_TREE_DEFAULTS'
      ,[ID],
    
	[E21] ,
	[E24] ,
	[E33] ,
	[E34] ,
	[E41] ,
	[E42] ,
	[E43] ,
	[E46] ,
	[E51] ,
	[E52] ,
	[E63] ,
	[E64] ,
	[E65] ,
	[E66] ,
	[E67] ,
	[E68] ,
	[E74] ,
	[E75] ,
	[E76] ,
	[E82] ,
	[E83] ,
	[E84] ,
	[E85] ,
	[E86] ,
	[E87] ,
	[E91] ,
	[E92] ,
	[E93] ,
	[E94] ,
	[E81] ,
	[E71] ,
	[E72] ,
	[E73] ,
	[E61] ,
	[E62] ,
	[E53] ,
	[E54] ,
	[E55] ,
	[E44] ,
	[E45] ,
	[E31] ,
	[E32] ,
	[E22] 
		 from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
      ,[PARENT_ID],
   
	[E21] ,
	[E24] ,
	[E33] ,
	[E34] ,
	[E41] ,
	[E42] ,
	[E43] ,
	[E46] ,
	[E51] ,
	[E52] ,
	[E63] ,
	[E64] ,
	[E65] ,
	[E66] ,
	[E67] ,
	[E68] ,
	[E74] ,
	[E75] ,
	[E76] ,
	[E82] ,
	[E83] ,
	[E84] ,
	[E85] ,
	[E86] ,
	[E87] ,
	[E91] ,
	[E92] ,
	[E93] ,
	[E94] ,
	[E81] ,
	[E71] ,
	[E72] ,
	[E73] ,
	[E61] ,
	[E62] ,
	[E53] ,
	[E54] ,
	[E55] ,
	[E44] ,
	[E45] ,
	[E31] ,
	[E32] ,
	[E22] 
    )
    SELECT
       'DELETE'
      ,'UTIL'
      ,'LU_TPD_FAULT_TREE_DEFAULTS'
      ,[ID],
    
    [E21] ,
	[E24] ,
	[E33] ,
	[E34] ,
	[E41] ,
	[E42] ,
	[E43] ,
	[E46] ,
	[E51] ,
	[E52] ,
	[E63] ,
	[E64] ,
	[E65] ,
	[E66] ,
	[E67] ,
	[E68] ,
	[E74] ,
	[E75] ,
	[E76] ,
	[E82] ,
	[E83] ,
	[E84] ,
	[E85] ,
	[E86] ,
	[E87] ,
	[E91] ,
	[E92] ,
	[E93] ,
	[E94] ,
	[E81] ,
	[E71] ,
	[E72] ,
	[E73] ,
	[E61] ,
	[E62] ,
	[E53] ,
	[E54] ,
	[E55] ,
	[E44] ,
	[E45] ,
	[E31] ,
	[E32] ,
	[E22] 	
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [UTIL_HIST].[LU_TPD_FAULT_TREE_DEFAULTS] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
      ,[PARENT_ID],
     

  	[E21] ,
	[E24] ,
	[E33] ,
	[E34] ,
	[E41] ,
	[E42] ,
	[E43] ,
	[E46] ,
	[E51] ,
	[E52] ,
	[E63] ,
	[E64] ,
	[E65] ,
	[E66] ,
	[E67] ,
	[E68] ,
	[E74] ,
	[E75] ,
	[E76] ,
	[E82] ,
	[E83] ,
	[E84] ,
	[E85] ,
	[E86] ,
	[E87] ,
	[E91] ,
	[E92] ,
	[E93] ,
	[E94] ,
	[E81] ,
	[E71] ,
	[E72] ,
	[E73] ,
	[E61] ,
	[E62] ,
	[E53] ,
	[E54] ,
	[E55] ,
	[E44] ,
	[E45] ,
	[E31] ,
	[E32] ,
	[E22] 
	)
    SELECT
       'UPDATE'
      ,'UTIL'
      ,'LU_TPD_FAULT_TREE_DEFAULTS'
      ,[ID],
   

    	[E21] ,
	[E24] ,
	[E33] ,
	[E34] ,
	[E41] ,
	[E42] ,
	[E43] ,
	[E46] ,
	[E51] ,
	[E52] ,
	[E63] ,
	[E64] ,
	[E65] ,
	[E66] ,
	[E67] ,
	[E68] ,
	[E74] ,
	[E75] ,
	[E76] ,
	[E82] ,
	[E83] ,
	[E84] ,
	[E85] ,
	[E86] ,
	[E87] ,
	[E91] ,
	[E92] ,
	[E93] ,
	[E94] ,
	[E81] ,
	[E71] ,
	[E72] ,
	[E73] ,
	[E61] ,
	[E62] ,
	[E53] ,
	[E54] ,
	[E55] ,
	[E44] ,
	[E45] ,
	[E31] ,
	[E32] ,
	[E22] 
	from deleted
    RETURN;
  END
END;


GO
ALTER TABLE [UTIL].[LU_TPD_FAULT_TREE_DEFAULTS] ENABLE TRIGGER [TRG_TRACK_HIST_LU_TPD_FAULT_TREE_DEFAULTS]
GO
