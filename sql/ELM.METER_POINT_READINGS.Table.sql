USE [PI_DW_DEV]
GO
/****** Object:  Table [ELM].[METER_POINT_READINGS]    Script Date: 11/4/2021 9:12:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ELM].[METER_POINT_READINGS](
	[KEY] [int] IDENTITY(1,1) NOT NULL,
	[CreatedTimestamp] [datetime2](7) NULL,
	[RowEffectiveDate] [date] NULL,
	[RowExpirationDate] [date] NULL,
	[RowCurrentIndicator] [bit] NULL,
	[MD5] [nvarchar](32) NOT NULL,
	[pt_id_nbr] [int] NOT NULL,
	[ReadingYear] [int] NULL,
	[ReadingMonth] [int] NULL,
	[attr_cd] [nvarchar](15) NULL,
	[Statistic] [nvarchar](5) NULL,
	[Value] [numeric](20, 3) NULL,
PRIMARY KEY CLUSTERED 
(
	[KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [ELM].[METER_POINT_READINGS] ADD  CONSTRAINT [DF_CREATEDTIMESTAMP_ELM_METER_POINT_READINGS]  DEFAULT (getdate()) FOR [CreatedTimestamp]
GO
