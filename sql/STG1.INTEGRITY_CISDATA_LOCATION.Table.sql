USE [PI_QRA_DEV]
GO
/****** Object:  Table [STG1].[INTEGRITY_CISDATA_LOCATION]    Script Date: 11/23/2021 1:51:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[INTEGRITY_CISDATA_LOCATION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[CISInspectionRangeEventID] [uniqueidentifier] NULL,
	[PipeSeriesID] [nvarchar](18) NULL,
	[AlignedStation] [numeric](12, 2) NULL,
	[X_Reported] [numeric](38, 8) NULL,
	[Y_Reported] [numeric](38, 8) NULL,
	[InspectionDate] [date] NULL,
	[GEOM] [geometry] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [STG1].[INTEGRITY_CISDATA_LOCATION] ADD  CONSTRAINT [DF_FAILED_INTEGRITY_CISDATA_LOCATION]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[INTEGRITY_CISDATA_LOCATION] ADD  CONSTRAINT [DF_PULLED_INTEGRITY_CISDATA_LOCATION]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_INTEGRITY_CISDATA_LOCATION]    Script Date: 11/23/2021 1:51:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--IF OBJECT_ID('[STG1].[TRG_TRACK_HIST_INTEGRITY_ILIDATA]') IS NOT NULL DROP TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_ILIDATA]

CREATE TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_CISDATA_LOCATION] ON [STG1].[INTEGRITY_CISDATA_LOCATION]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISDATA_LOCATION] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,PipeSeriesID,AlignedStation,X_Reported,Y_Reported,InspectionDate,GEOM
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'INTEGRITY_CISDATA_LOCATION'
	  ,ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,PipeSeriesID,AlignedStation,X_Reported,Y_Reported,InspectionDate,GEOM
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISDATA_LOCATION] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,PipeSeriesID,AlignedStation,X_Reported,Y_Reported,InspectionDate,GEOM
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'INTEGRITY_CISDATA_LOCATION'
	  ,ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,PipeSeriesID,AlignedStation,X_Reported,Y_Reported,InspectionDate,GEOM
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISDATA_LOCATION] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,PipeSeriesID,AlignedStation,X_Reported,Y_Reported,InspectionDate,GEOM
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'INTEGRITY_CISDATA_LOCATION'
	  ,ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,PipeSeriesID,AlignedStation,X_Reported,Y_Reported,InspectionDate,GEOM
	from deleted
    RETURN;
  END
END;

GO
ALTER TABLE [STG1].[INTEGRITY_CISDATA_LOCATION] ENABLE TRIGGER [TRG_TRACK_HIST_INTEGRITY_CISDATA_LOCATION]
GO
