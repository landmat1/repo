USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG1].[INTEGRITY_ACTIONABLEANOMALY]    Script Date: 11/16/2021 10:28:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[INTEGRITY_ACTIONABLEANOMALY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[Description] [nvarchar](4000) NULL,
	[Comments] [nvarchar](4000) NULL,
	[ILIDataEventID] [uniqueidentifier] NULL,
	[ILIDigNumber] [nvarchar](32) NULL,
	[DiscoveryDate] [datetime2](7) NULL,
	[BeginDigOdometer] [numeric](16, 3) NULL,
	[EndDigOdometer] [numeric](16, 3) NULL,
	[BeginDigStation] [numeric](16, 3) NULL,
	[EndDigStation] [numeric](16, 3) NULL,
	[DigLength] [numeric](9, 3) NULL,
	[VarianceRequiredLF] [nvarchar](25) NULL,
	[JustificationSubmittedLF] [nvarchar](25) NULL,
	[VarianceRefNumber] [nvarchar](32) NULL,
	[MOCRefNumber] [nvarchar](32) NULL,
	[PIStatusCL] [nvarchar](18) NULL,
	[ALGPriorityComments] [nvarchar](2000) NULL,
	[RStrengEFFAreaMaxSafePR] [numeric](16, 3) NULL,
	[ActualDF] [numeric](7, 3) NULL,
	[Algorithm] [nvarchar](100) NULL,
	[ResponseCriteria] [nvarchar](1000) NULL,
	[ResponseClass] [nvarchar](100) NULL,
	[DueDate] [date] NULL,
	[AbsoluteOdometer] [numeric](16, 3) NULL,
	[KMAnomalyTypeCL] [nvarchar](100) NULL,
	[ResponseSchedule] [nvarchar](8) NULL,
	[Priority] [nvarchar](50) NULL,
	[ConsequenceClass] [nvarchar](100) NULL,
	[AlternativeResponsePlanLF] [nvarchar](18) NULL,
	[TechnicalJustificationLF] [nvarchar](18) NULL,
	[ReportNumber] [nvarchar](255) NULL,
	[ServiceProviderJobNumber] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1].[INTEGRITY_ACTIONABLEANOMALY] ADD  CONSTRAINT [DF_FAILED_INTEGRITY_ACTIONABLEANOMALY]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[INTEGRITY_ACTIONABLEANOMALY] ADD  CONSTRAINT [DF_PULLED_INTEGRITY_ACTIONABLEANOMALY]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_INTEGRITY_ACTIONABLEANOMALY]    Script Date: 11/16/2021 10:28:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--IF OBJECT_ID('[STG1].[TRG_TRACK_HIST_INTEGRITY_ACTIONABLEANOMALY]') IS NOT NULL DROP TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_ACTIONABLEANOMALY]

CREATE TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_ACTIONABLEANOMALY] ON [STG1].[INTEGRITY_ACTIONABLEANOMALY]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,Comments,ILIDataEventID,ILIDigNumber,DiscoveryDate,BeginDigOdometer,EndDigOdometer,BeginDigStation,EndDigStation,DigLength,VarianceRequiredLF,JustificationSubmittedLF,VarianceRefNumber,MOCRefNumber,PIStatusCL,ALGPriorityComments,RStrengEFFAreaMaxSafePR,ActualDF,Algorithm,ResponseCriteria,ResponseClass,DueDate,AbsoluteOdometer,KMAnomalyTypeCL,ResponseSchedule,Priority,ConsequenceClass,AlternativeResponsePlanLF,TechnicalJustificationLF,ReportNumber,ServiceProviderJobNumber
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'INTEGRITY_ACTIONABLEANOMALY'
	  ,ID,FAILED,PULLED,EventID,Description,Comments,ILIDataEventID,ILIDigNumber,DiscoveryDate,BeginDigOdometer,EndDigOdometer,BeginDigStation,EndDigStation,DigLength,VarianceRequiredLF,JustificationSubmittedLF,VarianceRefNumber,MOCRefNumber,PIStatusCL,ALGPriorityComments,RStrengEFFAreaMaxSafePR,ActualDF,Algorithm,ResponseCriteria,ResponseClass,DueDate,AbsoluteOdometer,KMAnomalyTypeCL,ResponseSchedule,Priority,ConsequenceClass,AlternativeResponsePlanLF,TechnicalJustificationLF,ReportNumber,ServiceProviderJobNumber
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,Comments,ILIDataEventID,ILIDigNumber,DiscoveryDate,BeginDigOdometer,EndDigOdometer,BeginDigStation,EndDigStation,DigLength,VarianceRequiredLF,JustificationSubmittedLF,VarianceRefNumber,MOCRefNumber,PIStatusCL,ALGPriorityComments,RStrengEFFAreaMaxSafePR,ActualDF,Algorithm,ResponseCriteria,ResponseClass,DueDate,AbsoluteOdometer,KMAnomalyTypeCL,ResponseSchedule,Priority,ConsequenceClass,AlternativeResponsePlanLF,TechnicalJustificationLF,ReportNumber,ServiceProviderJobNumber
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'INTEGRITY_ACTIONABLEANOMALY'
	  ,ID,FAILED,PULLED,EventID,Description,Comments,ILIDataEventID,ILIDigNumber,DiscoveryDate,BeginDigOdometer,EndDigOdometer,BeginDigStation,EndDigStation,DigLength,VarianceRequiredLF,JustificationSubmittedLF,VarianceRefNumber,MOCRefNumber,PIStatusCL,ALGPriorityComments,RStrengEFFAreaMaxSafePR,ActualDF,Algorithm,ResponseCriteria,ResponseClass,DueDate,AbsoluteOdometer,KMAnomalyTypeCL,ResponseSchedule,Priority,ConsequenceClass,AlternativeResponsePlanLF,TechnicalJustificationLF,ReportNumber,ServiceProviderJobNumber
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_ACTIONABLEANOMALY] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,Comments,ILIDataEventID,ILIDigNumber,DiscoveryDate,BeginDigOdometer,EndDigOdometer,BeginDigStation,EndDigStation,DigLength,VarianceRequiredLF,JustificationSubmittedLF,VarianceRefNumber,MOCRefNumber,PIStatusCL,ALGPriorityComments,RStrengEFFAreaMaxSafePR,ActualDF,Algorithm,ResponseCriteria,ResponseClass,DueDate,AbsoluteOdometer,KMAnomalyTypeCL,ResponseSchedule,Priority,ConsequenceClass,AlternativeResponsePlanLF,TechnicalJustificationLF,ReportNumber,ServiceProviderJobNumber
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'INTEGRITY_ACTIONABLEANOMALY'
	  ,ID,FAILED,PULLED,EventID,Description,Comments,ILIDataEventID,ILIDigNumber,DiscoveryDate,BeginDigOdometer,EndDigOdometer,BeginDigStation,EndDigStation,DigLength,VarianceRequiredLF,JustificationSubmittedLF,VarianceRefNumber,MOCRefNumber,PIStatusCL,ALGPriorityComments,RStrengEFFAreaMaxSafePR,ActualDF,Algorithm,ResponseCriteria,ResponseClass,DueDate,AbsoluteOdometer,KMAnomalyTypeCL,ResponseSchedule,Priority,ConsequenceClass,AlternativeResponsePlanLF,TechnicalJustificationLF,ReportNumber,ServiceProviderJobNumber
	from deleted
    RETURN;
  END
END;


GO
ALTER TABLE [STG1].[INTEGRITY_ACTIONABLEANOMALY] ENABLE TRIGGER [TRG_TRACK_HIST_INTEGRITY_ACTIONABLEANOMALY]
GO
