USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG3].[DIM_CASING]    Script Date: 11/2/2021 10:54:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG3].[DIM_CASING](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[KEY] [int] NULL,
	[OutsideDiameter] [numeric](8, 3) NULL,
	[CrossingType] [nvarchar](50) NULL,
	[CasingMaterial] [nvarchar](20) NULL,
	[VentedIndicatorLF] [nvarchar](5) NULL,
	[BASIS] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
