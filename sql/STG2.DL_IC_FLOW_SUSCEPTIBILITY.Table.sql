USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG2].[DL_IC_FLOW_SUSCEPTIBILITY]    Script Date: 11/8/2021 7:08:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_IC_FLOW_SUSCEPTIBILITY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[P_GeometrySusceptibility] [numeric](5, 4) NULL,
	[P_LowPointSusceptibility] [numeric](5, 4) NULL,
	[P_StagnantFlowSusceptibility] [numeric](5, 4) NULL,
	[P_FlowSusceptibility] [numeric](5, 4) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
