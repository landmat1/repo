USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG1].[ELM_METER_POINT]    Script Date: 11/5/2021 10:39:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[ELM_METER_POINT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[Pin_Number] [int] NOT NULL,
	[MTR_PT_Status] [nvarchar](10) NULL,
	[KMI_Operator_Code1] [nvarchar](5) NULL,
	[KMI_Operator_Name1] [nvarchar](50) NULL,
	[KMI_Operator_Code2] [nvarchar](5) NULL,
	[KMI_Operator_Name2] [nvarchar](50) NULL,
	[MeterName] [nvarchar](100) NULL,
	[MeterDescription] [nvarchar](100) NULL,
	[PointTypeCode] [nvarchar](10) NULL,
	[PointTypeDescription] [nvarchar](100) NULL,
	[FirstDeliveryDate] [date] NULL,
	[CompanyName1] [nvarchar](100) NULL,
	[CompanyName2] [nvarchar](100) NULL,
	[Comment] [nvarchar](max) NULL,
	[PointMaxAllowableOperatingPressure] [int] NULL,
	[Latitude] [numeric](12, 8) NULL,
	[Longitude] [numeric](12, 8) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [STG1].[ELM_METER_POINT] ADD  CONSTRAINT [DF_FAILED_ELM_METER_POINT]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[ELM_METER_POINT] ADD  CONSTRAINT [DF_PULLED_ELM_METER_POINT]  DEFAULT (getdate()) FOR [PULLED]
GO
