USE [PI_DW_DEV]
GO
/****** Object:  Table [INTEGRITY].[CIS_INSPECTION_RANGE]    Script Date: 12/3/2021 9:32:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [INTEGRITY].[CIS_INSPECTION_RANGE](
	[KEY] [int] IDENTITY(1,1) NOT NULL,
	[CreatedTimestamp] [datetime2](7) NULL,
	[RowEffectiveDate] [date] NULL,
	[RowExpirationDate] [date] NULL,
	[RowCurrentIndicator] [bit] NULL,
	[MD5] [nvarchar](32) NOT NULL,
	[CISInspectionEventID] [uniqueidentifier] NULL,
	[CISInspectionRangeEventID] [uniqueidentifier] NULL,
	[CISInspectionRangeDescription] [nvarchar](4000) NULL,
	[CISInspectionRangeComments] [nvarchar](4000) NULL,
	[RouteEventID] [uniqueidentifier] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[BeginSeriesEventID] [uniqueidentifier] NULL,
	[EndSeriesEventID] [uniqueidentifier] NULL,
	[CISInspectionRangeBeginStation] [numeric](15, 2) NULL,
	[CISInspectionRangeEndStation] [numeric](15, 2) NULL,
	[CISSurveyTypeCL] [nvarchar](18) NULL,
	[InspectionBeginDate] [datetime2](7) NULL,
	[InspectionEndDate] [datetime] NULL,
	[BeginLatitude] [numeric](38, 8) NULL,
	[EndLatitude] [numeric](38, 8) NULL,
	[BeginLongitude] [numeric](38, 8) NULL,
	[EndLongitude] [numeric](38, 8) NULL,
PRIMARY KEY CLUSTERED 
(
	[KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [INTEGRITY].[CIS_INSPECTION_RANGE] ADD  CONSTRAINT [DF_CREATEDTIMESTAMP_CIS_INSPECTION_RANGE]  DEFAULT (getdate()) FOR [CreatedTimestamp]
GO
