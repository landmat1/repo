USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG2].[DL_GAS_COMPOSITION_CORROSION_EXPOSURE]    Script Date: 11/8/2021 12:05:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_GAS_COMPOSITION_CORROSION_EXPOSURE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[S_CO2] [numeric](3, 2) NULL,
	[CO2_SOURCE] [nvarchar](50) NULL,
	[S_H2O] [numeric](3, 2) NULL,
	[H2O_SOURCE] [nvarchar](50) NULL,
	[S_H2S] [numeric](3, 2) NULL,
	[H2S_SOURCE] [nvarchar](50) NULL,
	[S_O2] [numeric](3, 2) NULL,
	[O2_SOURCE] [nvarchar](50) NULL,
	[P_CompositionSusceptibility] [numeric](5, 4) NULL,
	[CORROSION_RATE] [numeric](9, 5) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
