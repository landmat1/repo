USE [PI_ODS_DEV]
GO

/****** Object:  Table [STG1].[PODS_PIPEBEND]    Script Date: 12/6/2021 3:13:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[PODS_PIPEBEND](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[Description] [nvarchar](4000) NULL,
	[SourceCL] [nvarchar](25) NULL,
	[Comments] [nvarchar](4000) NULL,
	[RouteEventID] [uniqueidentifier] NULL,
	[Measure] [numeric](15, 2) NULL,
	[HorizAngle] [numeric](7,3) NULL,
	[VertAngle] [numeric](7,3) NULL,
	[TechniqueCL] [nvarchar](50) NULL,
	[TypeCL] [nvarchar](50) NULL,
	[BendRadius] [nvarchar](50),
	[COMP_RM_GEOM]  AS ([geometry]::STPointFromText(('POINT ('+CONVERT([nvarchar],[Measure]))+' 0)',(0))),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1].[PODS_PIPEBEND] ADD  CONSTRAINT [DF_FAILED_PODS_PIPEBEND]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[PODS_PIPEBEND] ADD  CONSTRAINT [DF_PULLED_PODS_PIPEBEND]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_PODS_PIPEBEND]    Script Date: 12/6/2021 3:13:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--IF OBJECT_ID('[STG1].[TRG_TRACK_HIST_PODS_PIPEBEND]') IS NOT NULL DROP TRIGGER [STG1].[TRG_TRACK_HIST_PODS_PIPEBEND]

CREATE TRIGGER [STG1].[TRG_TRACK_HIST_PODS_PIPEBEND] ON [STG1].[PODS_PIPEBEND]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[PODS_PIPEBEND] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,SourceCL,Comments,RouteEventID,Measure,HorizAngle,VertAngle,TechniqueCL, TypeCL, BendRadius
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'PODS_PIPEBEND'
	  ,ID,FAILED,PULLED,EventID,Description,SourceCL,Comments,RouteEventID,Measure,HorizAngle,VertAngle,TechniqueCL, TypeCL, BendRadius
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[PODS_PIPEBEND] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,SourceCL,Comments,RouteEventID,Measure,HorizAngle,VertAngle,TechniqueCL, TypeCL, BendRadius
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'PODS_PIPEBEND'
	  ,ID,FAILED,PULLED,EventID,Description,SourceCL,Comments,RouteEventID,Measure,HorizAngle,VertAngle,TechniqueCL, TypeCL, BendRadius
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[PODS_PIPEBEND] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,Description,SourceCL,Comments,RouteEventID,Measure,HorizAngle,VertAngle,TechniqueCL, TypeCL, BendRadius
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'PODS_PIPEBEND'
	  ,ID,FAILED,PULLED,EventID,Description,SourceCL,Comments,RouteEventID,Measure,HorizAngle,VertAngle,TechniqueCL, TypeCL, BendRadius
	from deleted
    RETURN;
  END
END;


GO
ALTER TABLE [STG1].[PODS_PIPEBEND] ENABLE TRIGGER [TRG_TRACK_HIST_PODS_PIPEBEND]
GO
