USE [PI_ODS_DEV]
GO

/****** Object:  Table [STG2].[DL_LINE_PACK]    Script Date: 12/7/2021 9:03:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [STG2].[DL_PIPEBEND](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[Comments] [nvarchar](4000) NULL,
    [HorizAngle] [numeric](7, 3) NULL,
	[VertAngle] [numeric](7, 3) NULL,
	[TechniqueCL] [nvarchar](50) NULL,
	[TypeCL] [nvarchar](50) NULL,
	[BendRadius] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


