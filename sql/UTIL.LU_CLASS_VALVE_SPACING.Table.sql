USE [PI_ODS_DEV]
GO
/****** Object:  Table [UTIL].[LU_CLASS_VALVE_SPACING]    Script Date: 11/3/2021 3:22:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UTIL].[LU_CLASS_VALVE_SPACING](
	[UUID] [uniqueidentifier] NOT NULL,
	[CLASS_LOCATION] [nvarchar](10) NOT NULL,
	[VALVE_SPACING_MILE] [numeric](5, 1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
