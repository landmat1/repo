USE [PI_ODS_DEV]
GO
/****** Object:  View [STG3].[DIM_SLEEVE_RANGE_VW]    Script Date: 11/2/2021 10:54:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [STG3].[DIM_SLEEVE_RANGE_VW] AS

SELECT
	b.[ID],
	b.[ROUTE_ID],
	b.[BeginMeasure],
	b.[EndMeasure],
    a.[KEY],
	a.[SleeveType],
	a.[SleeveType_BASIS]
FROM [STG3].[DIM_SLEEVE] a
INNER JOIN [STG3].[DIM_SLEEVE_RANGE] b on b.[SLEEVE_KEY] = a.[KEY]
GO
