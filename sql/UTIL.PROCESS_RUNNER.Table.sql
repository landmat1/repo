
GO
/****** Object:  Table [UTIL].[PROCESS_RUNNER]    Script Date: 11/2/2021 11:36:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UTIL].[PROCESS_RUNNER](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[THREAT] [nvarchar](20) NULL,
	[RUN_UUID] [uniqueidentifier] NULL,
	[PROCESS_NAME] [nvarchar](200) NULL,
	[StartTime] [datetime2](7) NULL,
	[EndTime] [datetime2](7) NULL,
	[RunTime_s] [numeric](20, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
