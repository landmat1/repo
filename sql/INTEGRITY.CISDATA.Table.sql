USE [PI_DW_DEV]
GO
/****** Object:  Table [INTEGRITY].[CISDATA]    Script Date: 12/3/2021 11:53:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [INTEGRITY].[CISDATA](
	[KEY] [int] IDENTITY(1,1) NOT NULL,
	[CreatedTimestamp] [datetime2](7) NULL,
	[RowEffectiveDate] [date] NULL,
	[RowExpirationDate] [date] NULL,
	[RowCurrentIndicator] [bit] NULL,
	[EventID] [uniqueidentifier] NULL,
	[CISInspectionRangeEventID] [uniqueidentifier] NULL,
	[PipeSeriesID] [nvarchar](18) NULL,
	[X_Reported] [numeric](12, 8) NULL,
	[Y_Reported] [numeric](12, 8) NULL,
	[AlignedStation] [numeric](12,2) NULL,
	[PSOff] [numeric](7, 3) NULL,
	[PipeDepthIn] [numeric](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [INTEGRITY].[CISDATA] ADD  CONSTRAINT [DF_CREATEDTIMESTAMP_CISDATA]  DEFAULT (getdate()) FOR [CreatedTimestamp]
GO
