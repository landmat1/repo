USE [PI_ODS_DEV]
GO
/****** Object:  Table [UTIL].[LU_PRODUCT_TRANSPORTATION_VALUE]    Script Date: 11/3/2021 3:22:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UTIL].[LU_PRODUCT_TRANSPORTATION_VALUE](
	[UUID] [uniqueidentifier] NOT NULL,
	[PODS_OperatorCL] [nvarchar](18) NULL,
	[PODS_OwnerCL] [nvarchar](18) NULL,
	[FINANCIAL_PLANNING_OPERATOR] [nvarchar](100) NULL,
	[TRASPORTATION_VALUE_DOLLAR_DTH] [numeric](10, 4) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
