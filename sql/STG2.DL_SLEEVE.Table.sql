USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG2].[DL_SLEEVE]    Script Date: 11/2/2021 10:54:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG2].[DL_SLEEVE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ROUTE_ID] [int] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[InstallationDate] [date] NULL,
	[Material] [nvarchar](50) NULL,
	[NominalWallThickness] [numeric](7,4) NULL,
	[SleeveType] [nvarchar](50) NULL,
	[SleeveManufacturer] [nvarchar](50) NULL,
	[SleeveType_BASIS] [nvarchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
