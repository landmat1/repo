USE [PI_ODS_DEV]
GO
/****** Object:  Table [UTIL].[LU_PIPE_SCHEDULE]    Script Date: 11/4/2021 4:41:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [UTIL].[LU_PIPE_SCHEDULE](
	[UUID] [uniqueidentifier] NOT NULL,
	[Material] [nvarchar](50) NULL,
	[Specification] [nvarchar](10) NOT NULL,
	[Schedule] [nvarchar](10) NOT NULL,
	[OutsideDiameter] [numeric](8, 4) NULL,
	[WallThickness] [numeric](8, 4) NULL,
	[MAOP] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
