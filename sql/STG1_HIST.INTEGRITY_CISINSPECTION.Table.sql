USE [PI_QRA_DEV]
GO
/****** Object:  Table [STG1_HIST].[INTEGRITY_CISINSPECTION]    Script Date: 11/9/2021 7:26:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1_HIST].[INTEGRITY_CISINSPECTION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CHG_DATE] [datetime] NOT NULL,
	[CHG_TYPE] [nvarchar](20) NOT NULL,
	[CHG_BY] [nvarchar](256) NOT NULL,
	[APP_NAME] [nvarchar](128) NOT NULL,
	[HOST_NAME] [nvarchar](128) NOT NULL,
	[SCHEMA_NAME] [sysname] NOT NULL,
	[OBJECT_NAME] [sysname] NOT NULL,
	[PARENT_ID] [int] NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[Description] [nvarchar](4000) NULL,
	[Comments] [nvarchar](4000) NULL,
	[AssessmentEventID] [uniqueidentifier] NULL,
	[SourceFile] [nvarchar](255) NULL,
	[BeginDate] [date] NULL,
	[EndDate] [date] NULL,
	[AlignmentDate] [date] NULL,
	[InspectionBeginDate] [date] NULL,
	[InspectionEndDate] [date] NULL,
	[InspectionTypeCL] [nvarchar](18) NULL,
	[CISConnectionType] [nvarchar](255) NULL,
	[BeginLatitude] [numeric](38,8) NULL,
	[BeginLongitude] [numeric](38,8) NULL,
	[EndLatitude] [numeric](38,8) NULL,
	[EndLongitude] [numeric](38,8) NULL,
	[IntCycleOn] [numeric](15,4) NULL,
	[IntCycleOff] [numeric](15,4) NULL,
	[DataLoadedLF] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISINSPECTION] ADD  CONSTRAINT [DF_HIST_CHG_DATE_INTEGRITY_CISINSPECTION]  DEFAULT (getdate()) FOR [CHG_DATE]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISINSPECTION] ADD  CONSTRAINT [DF_HIST_CHG_TYPE_INTEGRITY_CISINSPECTION]  DEFAULT ('') FOR [CHG_TYPE]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISINSPECTION] ADD  CONSTRAINT [DF_HIST_CHG_BY_INTEGRITY_CISINSPECTION]  DEFAULT (coalesce(suser_sname(),'?')) FOR [CHG_BY]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISINSPECTION] ADD  CONSTRAINT [DF_HIST_APP_NAME_INTEGRITY_CISINSPECTION]  DEFAULT (coalesce(app_name(),'?')) FOR [APP_NAME]
GO
ALTER TABLE [STG1_HIST].[INTEGRITY_CISINSPECTION] ADD  CONSTRAINT [DF_HIST_HOST_NAME_INTEGRITY_CISINSPECTION]  DEFAULT (coalesce(host_name(),'?')) FOR [HOST_NAME]
GO
