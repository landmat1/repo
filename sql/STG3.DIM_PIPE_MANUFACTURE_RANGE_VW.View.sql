USE [PI_ODS_DEV]
GO
/****** Object:  View [STG3].[DIM_PIPE_MANUFACTURE_RANGE_VW]    Script Date: 11/2/2021 10:10:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [STG3].[DIM_PIPE_MANUFACTURE_RANGE_VW] AS

SELECT
	b.[ID],
	b.[ROUTE_ID],
	b.[BeginMeasure],
	b.[EndMeasure],
	a.[PIPE_MANUFACTURE_KEY],
    a.[Material],
    a.[Manufacturer],
    a.[Specification],
    a.[NominalOutsideDiameter],
	a.[InsideDiameter],
    a.[NominalWallThickness],
	a.[DT_Ratio],
    a.[Grade],
    a.[SMYS],
    a.[SMYS10K],
    a.[LongSeam],
    a.[MillLocation]
FROM [STG3].[DIM_PIPE_MANUFACTURE] a
INNER JOIN [STG3].[DIM_PIPE_MANUFACTURE_RANGE] b on b.[PIPE_MANUFACTURE_KEY] = a.[PIPE_MANUFACTURE_KEY]

GO
