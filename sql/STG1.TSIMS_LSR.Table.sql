USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG1].[TSIMS_LSR]    Script Date: 11/9/2021 10:53:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[TSIMS_LSR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[SamplePtID] [int] NULL,
	[RecID] [int] NULL,
	[LSRLogicCode] [nvarchar](50) NULL,
	[TSIMS_LSR_Logic] [nvarchar](2000) NULL,
	[LSRApprovedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1].[TSIMS_LSR] ADD  CONSTRAINT [DF_FAILED_TSIMS_LSR]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[TSIMS_LSR] ADD  CONSTRAINT [DF_PULLED_TSIMS_LSR]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_TSIMS_LSR]    Script Date: 11/9/2021 10:53:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [STG1].[TRG_TRACK_HIST_TSIMS_LSR] ON [STG1].[TSIMS_LSR]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[TSIMS_LSR] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,SamplePtID,RecID,LSRLogicCode,TSIMS_LSR_Logic,LSRApprovedDate
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'TSIMS_LSR'
	  ,ID,FAILED,PULLED,SamplePtID,RecID,LSRLogicCode,TSIMS_LSR_Logic,LSRApprovedDate
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[TSIMS_LSR] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,SamplePtID,RecID,LSRLogicCode,TSIMS_LSR_Logic,LSRApprovedDate
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'TSIMS_LSR'
	  ,ID,FAILED,PULLED,SamplePtID,RecID,LSRLogicCode,TSIMS_LSR_Logic,LSRApprovedDate
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[TSIMS_LSR] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,SamplePtID,RecID,LSRLogicCode,TSIMS_LSR_Logic,LSRApprovedDate
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'TSIMS_LSR'
	  ,ID,FAILED,PULLED,SamplePtID,RecID,LSRLogicCode,TSIMS_LSR_Logic,LSRApprovedDate
	from deleted
    RETURN;
  END
END;



ALTER TABLE [STG1].[TSIMS_LSR] ENABLE TRIGGER [TRG_TRACK_HIST_TSIMS_LSR]
GO
ALTER TABLE [STG1].[TSIMS_LSR] ENABLE TRIGGER [TRG_TRACK_HIST_TSIMS_LSR]
GO
