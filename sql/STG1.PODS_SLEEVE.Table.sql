USE [PI_ODS_DEV]
GO
/****** Object:  Table [STG1].[PODS_SLEEVE]    Script Date: 11/2/2021 10:54:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[PODS_SLEEVE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[RouteEventID] [uniqueidentifier] NULL,
	[BeginMeasure] [numeric](15, 2) NULL,
	[EndMeasure] [numeric](15, 2) NULL,
	[Description] [nvarchar](4000) NULL,
	[InstallationDate] [datetime2](7) NULL,
	[PipeGradeCL] [nvarchar](18) NULL,
	[DateManufactured] [datetime2](7) NULL,
	[ManufacturerCL] [nvarchar](50) NULL,
	[MaterialCL] [nvarchar](50) NULL,
	[MillTestPressure] [int] NULL,
	[NominalPressureRating] [int] NULL,
	[NominalWallThicknessCL] [numeric](7, 4) NULL,
	[NominalDiameterCL] [numeric](8, 4) NULL,
	[SpecificationCL] [nvarchar](50) NULL,
	[TypeCL] [nvarchar](50) NULL,
	[COMP_RM_GEOM]  AS ([geometry]::STLineFromText(((('LINESTRING ('+CONVERT([nvarchar],[BeginMeasure]))+' 0, ')+CONVERT([nvarchar],[EndMeasure]))+' 0)',(0))),
	[COMP_RM_SLOPE]  AS (case when [BeginMeasure]<[EndMeasure] then (1) when [BeginMeasure]=[EndMeasure] then (0) when [BeginMeasure]>[EndMeasure] then (-1)  end),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [STG1].[PODS_SLEEVE] ADD  CONSTRAINT [DF_FAILED_PODS_SLEEVE]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[PODS_SLEEVE] ADD  CONSTRAINT [DF_PULLED_PODS_SLEEVE]  DEFAULT (getdate()) FOR [PULLED]
GO
