USE [PI_QRA_DEV]
GO
/****** Object:  Table [STG1].[INTEGRITY_CISDATA]    Script Date: 11/22/2021 3:32:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG1].[INTEGRITY_CISDATA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FAILED] [bit] NULL,
	[PULLED] [datetime2](7) NULL,
	[EventID] [uniqueidentifier] NULL,
	[CISInspectionRangeEventID] [uniqueidentifier] NULL,
	[RouteEventID] [uniqueidentifier] NULL,
	[Measure] [numeric](15, 2) NULL,
	[InspectionDate] [date] NULL,
	[OffsetDistance] [numeric](7,2) NULL,
	[OffsetDirectionCL] [nvarchar](18) NULL,
	[NearGroundOn] [numeric](7, 3) NULL,
	[NearGroundOff] [numeric](7, 3) NULL,
	[FarGroundOn] [numeric](7, 3) NULL,
	[FarGroundOff] [numeric](7, 3) NULL,
	[PSOn] [numeric](7, 3) NULL,
	[PSOff] [numeric](7, 3) NULL,
	[RLATOn] [numeric](7, 3) NULL,
	[RLATOff] [numeric](7, 3) NULL,
	[LLATOn] [numeric](7, 3) NULL,
	[LLATOff] [numeric](7, 3) NULL,
	[MIROn] [numeric](7, 3) NULL,
	[MIROff] [numeric](7, 3) NULL,
	[MIRDepol] [numeric](7, 3) NULL,
	[CASOn] [numeric](7, 3) NULL,
	[CASOff] [numeric](7, 3) NULL,
	[CASDepol] [numeric](7, 3) NULL,
	[FOROn] [numeric](7, 3) NULL,
	[FOROff] [numeric](7, 3) NULL,
	[FORDepol] [numeric](7, 3) NULL,
	[FarGroundDepol] [numeric](7, 3) NULL,
	[FarGroundAC] [numeric](7, 3) NULL,
	[LLATDepol] [numeric](7, 3) NULL,
	[NearGroundDepol] [numeric](7, 3) NULL,
	[NearGroundAC] [numeric](7, 3) NULL,
	[PSDepol] [numeric](7, 3) NULL,
	[PSAC] [numeric](7, 3) NULL,
	[RlatDepol] [numeric](7, 3) NULL,
	[POINT_X] [numeric](38, 8) NULL,
	[POINT_Y] [numeric](38, 8) NULL,
	[POINT_Z] [numeric](38, 8) NULL,
	[X_Reported] [numeric](38, 8) NULL,
	[Y_Reported] [numeric](38, 8) NULL,
	[Z_Reported] [numeric](38, 8) NULL,
	[GEOM] [geometry] NULL,
	[COMP_RM_GEOM]  AS ([geometry]::STPointFromText(('POINT ('+CONVERT([nvarchar],[Measure]))+' 0)',(0))),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [STG1].[INTEGRITY_CISDATA] ADD  CONSTRAINT [DF_FAILED_INTEGRITY_CISDATA]  DEFAULT ((0)) FOR [FAILED]
GO
ALTER TABLE [STG1].[INTEGRITY_CISDATA] ADD  CONSTRAINT [DF_PULLED_INTEGRITY_CISDATA]  DEFAULT (getdate()) FOR [PULLED]
GO
/****** Object:  Trigger [STG1].[TRG_TRACK_HIST_INTEGRITY_CISDATA]    Script Date: 11/22/2021 3:32:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--IF OBJECT_ID('[STG1].[TRG_TRACK_HIST_INTEGRITY_CISDATA]') IS NOT NULL DROP TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_CISDATA]

CREATE TRIGGER [STG1].[TRG_TRACK_HIST_INTEGRITY_CISDATA] ON [STG1].[INTEGRITY_CISDATA]
  FOR INSERT, UPDATE, DELETE AS
BEGIN

  -- Detect inserts
  IF EXISTS (select * from inserted) AND NOT EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISDATA] (
       [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,RouteEventID,Measure,InspectionDate,OffsetDistance,OffsetDirectionCL,NearGroundOn,NearGroundOff,FarGroundOn,FarGroundOff,PSOn,PSOff,RLATOn,RLATOff,LLATOn,LLATOff,MIROn,MIROff,MIRDepol,CASOn,CASOff,CASDepol,FOROn,FOROff,FORDepol,FarGroundDepol,FarGroundAC,LLATDepol,NearGroundDepol,NearGroundAC,PSDepol,PSAC,RlatDepol,POINT_X,POINT_Y,POINT_Z,X_Reported,Y_Reported,Z_Reported,GEOM
	  )
    SELECT
	   'INSERT'
	  ,'STG1'
	  ,'INTEGRITY_CISDATA'
	  ,ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,RouteEventID,Measure,InspectionDate,OffsetDistance,OffsetDirectionCL,NearGroundOn,NearGroundOff,FarGroundOn,FarGroundOff,PSOn,PSOff,RLATOn,RLATOff,LLATOn,LLATOff,MIROn,MIROff,MIRDepol,CASOn,CASOff,CASDepol,FOROn,FOROff,FORDepol,FarGroundDepol,FarGroundAC,LLATDepol,NearGroundDepol,NearGroundAC,PSDepol,PSAC,RlatDepol,POINT_X,POINT_Y,POINT_Z,X_Reported,Y_Reported,Z_Reported,GEOM
	from inserted
    RETURN;
  END
    
  -- Detect deletes
  IF EXISTS (select * from deleted) AND NOT EXISTS (select * from inserted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISDATA] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,RouteEventID,Measure,InspectionDate,OffsetDistance,OffsetDirectionCL,NearGroundOn,NearGroundOff,FarGroundOn,FarGroundOff,PSOn,PSOff,RLATOn,RLATOff,LLATOn,LLATOff,MIROn,MIROff,MIRDepol,CASOn,CASOff,CASDepol,FOROn,FOROff,FORDepol,FarGroundDepol,FarGroundAC,LLATDepol,NearGroundDepol,NearGroundAC,PSDepol,PSAC,RlatDepol,POINT_X,POINT_Y,POINT_Z,X_Reported,Y_Reported,Z_Reported,GEOM
	)
    SELECT
	   'DELETE'
	  ,'STG1'
	  ,'INTEGRITY_CISDATA'
	  ,ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,RouteEventID,Measure,InspectionDate,OffsetDistance,OffsetDirectionCL,NearGroundOn,NearGroundOff,FarGroundOn,FarGroundOff,PSOn,PSOff,RLATOn,RLATOff,LLATOn,LLATOff,MIROn,MIROff,MIRDepol,CASOn,CASOff,CASDepol,FOROn,FOROff,FORDepol,FarGroundDepol,FarGroundAC,LLATDepol,NearGroundDepol,NearGroundAC,PSDepol,PSAC,RlatDepol,POINT_X,POINT_Y,POINT_Z,X_Reported,Y_Reported,Z_Reported,GEOM
	from deleted
    RETURN;
  END

  -- Update inserts
  IF EXISTS (select * from inserted) AND EXISTS (select * from deleted)
  BEGIN
    INSERT [STG1_HIST].[INTEGRITY_CISDATA] (
	   [CHG_TYPE]
      ,[SCHEMA_NAME]
      ,[OBJECT_NAME]
	  ,PARENT_ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,RouteEventID,Measure,InspectionDate,OffsetDistance,OffsetDirectionCL,NearGroundOn,NearGroundOff,FarGroundOn,FarGroundOff,PSOn,PSOff,RLATOn,RLATOff,LLATOn,LLATOff,MIROn,MIROff,MIRDepol,CASOn,CASOff,CASDepol,FOROn,FOROff,FORDepol,FarGroundDepol,FarGroundAC,LLATDepol,NearGroundDepol,NearGroundAC,PSDepol,PSAC,RlatDepol,POINT_X,POINT_Y,POINT_Z,X_Reported,Y_Reported,Z_Reported,GEOM
	)
    SELECT
	   'UPDATE'
	  ,'STG1'
	  ,'INTEGRITY_CISDATA'
	  ,ID,FAILED,PULLED,EventID,CISInspectionRangeEventID,RouteEventID,Measure,InspectionDate,OffsetDistance,OffsetDirectionCL,NearGroundOn,NearGroundOff,FarGroundOn,FarGroundOff,PSOn,PSOff,RLATOn,RLATOff,LLATOn,LLATOff,MIROn,MIROff,MIRDepol,CASOn,CASOff,CASDepol,FOROn,FOROff,FORDepol,FarGroundDepol,FarGroundAC,LLATDepol,NearGroundDepol,NearGroundAC,PSDepol,PSAC,RlatDepol,POINT_X,POINT_Y,POINT_Z,X_Reported,Y_Reported,Z_Reported,GEOM
	from deleted
    RETURN;
  END
END;




GO
ALTER TABLE [STG1].[INTEGRITY_CISDATA] ENABLE TRIGGER [TRG_TRACK_HIST_INTEGRITY_CISDATA]
GO
